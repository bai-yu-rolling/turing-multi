package com.t1;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "test-start")
public class TestStartProperties {


    private String pro;

}
