package com.t1;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(TestStartService.class)
@EnableConfigurationProperties(TestStartProperties.class)
public class TestStartConfig {

    private final TestStartProperties testStartProperties;


    public TestStartConfig(TestStartProperties testStartProperties) {
        this.testStartProperties = testStartProperties;
    }

    @Bean
    @ConditionalOnMissingBean(TestStartService.class)
    public TestStartService testStartService(){
        return new TestStartService(testStartProperties.getPro());
    }


}
