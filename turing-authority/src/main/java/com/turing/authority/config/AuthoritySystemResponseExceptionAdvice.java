package com.turing.authority.config;

import cn.dev33.satoken.exception.*;
import com.turing.core.filter.ExceptionAdviceCustomizers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Configuration
public class AuthoritySystemResponseExceptionAdvice  {


    @Bean
    public ExceptionAdviceCustomizers exceptionAndStatusMapping() {
        return () -> new HashMap<Class<? extends Exception>, Integer>(){{
             put(NotPermissionException.class, HttpServletResponse.SC_FORBIDDEN);
             put(NotLoginException.class, HttpServletResponse.SC_FORBIDDEN);
             put(NotRoleException.class, HttpServletResponse.SC_FORBIDDEN);
             put(NotBasicAuthException.class, HttpServletResponse.SC_FORBIDDEN);
             put(NotSafeException.class, HttpServletResponse.SC_FORBIDDEN);
             put(SameTokenInvalidException.class, HttpServletResponse.SC_FORBIDDEN);
             put(SaSignException.class, HttpServletResponse.SC_FORBIDDEN);
        }};
    }
}
