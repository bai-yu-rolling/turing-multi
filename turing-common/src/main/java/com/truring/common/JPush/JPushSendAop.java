package com.truring.common.JPush;

import cn.hutool.core.bean.BeanException;
import cn.hutool.core.bean.DynaBean;
import cn.hutool.core.util.ReflectUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Aspect
@Component
@ConditionalOnProperty(prefix = "jpush", name = "enable-aop", havingValue = "true")
public class JPushSendAop {

    private static final Map<Class<?>, JPushParamStrategy> CLASS_J_PUSH_PARAM_STRATEGY_CONCURRENT_CACHE = new ConcurrentHashMap<>(64);

    private final JPushClientWrapper jPushClientWrapper;

    public JPushSendAop(JPushClientWrapper jPushClientWrapper) {
        this.jPushClientWrapper = jPushClientWrapper;
    }

    @Pointcut("@annotation(com.truring.common.JPush.JPushSendPoint)")
    public void pointcut(){

    }


    @After("pointcut()")
    public void afterJPush(JoinPoint point){
        log.debug("jpush aop start");
        Object[] args = point.getArgs();
        if (args.length <= 0){
            throw new IllegalArgumentException("JPush没有可用的参数");
        }
        JPushSendPoint jPushSendPoint = ((MethodSignature)point.getSignature()).getMethod().getAnnotation(JPushSendPoint.class);
        String titleKey = jPushSendPoint.titleKey();
        String contentKey = jPushSendPoint.contentKey();
        String audienceKey = jPushSendPoint.audienceKey();
        String type = jPushSendPoint.type();
        Class<? extends JPushParamStrategy> strategyClazz = jPushSendPoint.strategy();
        JPushParamStrategy strategy = buildStrategy(strategyClazz);

        log.debug("jpush aop 注解：titleKey:{},contentKey:{},audienceKey:{}",titleKey,contentKey, audienceKey);
        String title = null;
        String content = null;
        String [] audience = null;
        boolean hasTitle= false;
        boolean hasAudience= false;
        boolean hasContent= false;
        for (Object o: args) {
            if (o==null||o instanceof String||o instanceof Collection)continue;
            log.debug(o.toString());
            DynaBean dynaBean = new DynaBean(o);
            Object getter = getterNoThrow(dynaBean,titleKey);
            if (getter != null){
                title = strategy.title(getter);
                hasTitle = true;
            }
            getter = getterNoThrow(dynaBean,contentKey);
            if (getter != null){
                content = strategy.content(getter);
                hasContent = true;
            }
            getter = getterNoThrow(dynaBean,audienceKey);
            if (getter != null){
                audience = strategy.audience(getter);
                hasAudience = true;
            }
        }
        if (!hasTitle||!hasAudience||!hasContent){
            throw new IllegalArgumentException("参数中没有可用的属性");
        }
        if (jPushSendPoint.async()){
            String finalTitle = title;
            String finalContent = content;
            String[] finalAudience = audience;
            CompletableFuture.runAsync(()->{
                exec(type, finalTitle, finalContent, finalAudience);
            });
        }else {
            exec(type,title,content,audience);
        }

    }

    private void exec(String type, String title, String content, String... audience){
        switch (type){
            case JPushClientWrapper.ALIAS:
                jPushClientWrapper.sendPush(title,content,audience);
                break;
            case JPushClientWrapper.REGISTRATION_ID:
            case JPushClientWrapper.TAG:
            default:
                throw new IllegalStateException("JPush没有相关的方法去处理当前推送类型");
        }
    }



    private Object getterNoThrow(DynaBean dynaBean,String field){
        try {
            return dynaBean.get(field);
        }catch (BeanException e){
            return null;
        }
    }

    private static JPushParamStrategy buildStrategy(Class<? extends JPushParamStrategy> strategyClazz){
        JPushParamStrategy jPushParamStrategy =  CLASS_J_PUSH_PARAM_STRATEGY_CONCURRENT_CACHE.get(strategyClazz);
        if (jPushParamStrategy == null){
            jPushParamStrategy = ReflectUtil.newInstance(strategyClazz);
            CLASS_J_PUSH_PARAM_STRATEGY_CONCURRENT_CACHE.put(strategyClazz,jPushParamStrategy);
        }
        return jPushParamStrategy;
    }

}
