package com.truring.common.JPush;

public class JPushParamDefaultStrategy implements JPushParamStrategy{
    @Override
    public String[] audience(Object audience) {
        return (String[]) audience;
    }

    @Override
    public String title(Object title) {
        return (String) title;
    }

    @Override
    public String content(Object content) {
        return (String) content;
    }
}
