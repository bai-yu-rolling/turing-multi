package com.truring.common.JPush;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface JPushSendPoint {

    /**
     * 标题字段
     * @return
     */
    String titleKey();

    /**
     * 内容字段
     * @return
     */
    String contentKey();

    /**
     * 人员字段
     * @return
     */
    String audienceKey();

    /**
     * 推送类型
     * @return
     */
    String type() default JPushClientWrapper.ALIAS;

    boolean async() default false;

    /**
     * 参数解析策略
     * @return
     */
    Class<? extends JPushParamStrategy> strategy() default JPushParamDefaultStrategy.class;

}
