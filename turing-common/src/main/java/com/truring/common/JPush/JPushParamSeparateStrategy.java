package com.truring.common.JPush;


import org.apache.commons.lang3.StringUtils;

public class JPushParamSeparateStrategy implements JPushParamStrategy {

    @Override
    public String[] audience(Object audience) {
        String au = (String) audience;
        if (StringUtils.isBlank(au))return null;
        return au.split(",");
    }

    @Override
    public String title(Object title) {
        return (String) title;
    }

    @Override
    public String content(Object content) {
        return (String) content;
    }
}
