package com.truring.common.JPush;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jiguang.common.resp.DefaultResult;
import cn.jpush.api.JPushClient;
import cn.jpush.api.device.AliasDeviceListResult;
import cn.jpush.api.device.TagAliasResult;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosAlert;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;

import java.util.Arrays;
import java.util.Collections;

/**
 * 封装类
 */
@Slf4j
public class JPushClientWrapper {

    public final static String ALIAS = "alias";
    public final static String REGISTRATION_ID = "registrationId";
    public final static String TAG = "tag";


    protected JPushClient jPushClient;

    /**
     * 该字段仅对 iOS 的 Notification 有效，如果不指定则为推送生产环境。注意：JPush 服务端 SDK 默认设置为推送 “开发环境”。
     * true：表示推送生产环境。
     * false：表示推送开发环境。
     */
    protected static boolean production;

    public JPushClientWrapper(JPushClient jPushClient) {
        this.jPushClient = jPushClient;
    }

    public PushResult sendPush(PushPayload payload) throws APIRequestException, APIConnectionException {
        return jPushClient.sendPush(payload);
    }

    /**
     * jpush本身自带重试，这里的重试和jpush有所不同，jpush会有读超时和连接超时，只有连接超时才会重试
     * @param payload
     * @param doneRetriedTimes 重试次数
     * @return
     * @throws APIRequestException
     * @throws APIConnectionException
     */
    public PushResult sendPushRetry(PushPayload payload,int doneRetriedTimes) throws APIRequestException, APIConnectionException {
        int i = 0;
        APIConnectionException exception = null;
        while (i < doneRetriedTimes){
            try {
                return jPushClient.sendPush(payload);
            } catch (APIConnectionException e) {
                log.error("连接错误",e);
                if (e.isReadTimedout()){
                    //读超时不应该重试
                    throw new APIConnectionException("读超时",e);
                }else {
                    log.warn("连接超时重试次数:{}",++i);
                    if (i == doneRetriedTimes){
                        exception = e;
                    }
                }

            }
        }
        throw new APIConnectionException("超过重试次数:"+i,exception);
    }

    public TagAliasResult getDevices(String rid) throws APIConnectionException, APIRequestException {
        return jPushClient.getDeviceTagAlias(rid);
    }

    public DefaultResult setAlias(String rid, String alias) throws APIConnectionException, APIRequestException {
        return jPushClient.updateDeviceTagAlias(rid,alias,null,null);
    }

    public DefaultResult addTage(String rid, String tag) throws APIConnectionException, APIRequestException {
        return jPushClient.updateDeviceTagAlias(rid,null, Collections.singleton(tag),null);
    }

    public DefaultResult updateTage(String tag, java.util.Set<String> toAddUsers) throws APIConnectionException, APIRequestException {
        return jPushClient.addRemoveDevicesFromTag(tag,toAddUsers,null);
    }

    public DefaultResult deleteTage(String tag) throws APIConnectionException, APIRequestException {
        return jPushClient.deleteTag(tag,"android,ios,quickapp");
    }



    public AliasDeviceListResult getRidByAlias(String alias) throws APIConnectionException, APIRequestException {
        return jPushClient.getAliasDeviceList(alias,"android,ios,quickapp");
    }

    public DefaultResult clearAlias(String rid) throws APIConnectionException, APIRequestException {
        return jPushClient.updateDeviceTagAlias(rid,true,false);
    }


    /**
     *  获取请求体
     * @param title 标题
     * @param content 内容
     * @param audience 接收者
     * @return
     */
    public static PushPayload buildPushObject_all_audience_alert(String title,String content,Audience audience){

        PushPayload.Builder builder = PushPayload.newBuilder();
        builder
                .setPlatform(Platform.all())
                .setAudience(audience)
                .setNotification(
                        Notification.newBuilder()
                                .setAlert(content)
                                .addPlatformNotification(IosNotification.newBuilder().setAlert(
                                        IosAlert.newBuilder().setTitleAndBody(title,"",content).build()
                                ).build())
                                .addPlatformNotification(AndroidNotification.newBuilder().setTitle(title).setAlert(content).build())
                                .build()
                )
                .setOptions(Options.newBuilder().setApnsProduction(production).build());
        return builder.build();

    }

    public static PushPayload buildPushObject_all_rid_alert(String title,String content,String ...rid){
        return buildPushObject_all_audience_alert(title,content,Audience.registrationId(rid));
    }

    public static PushPayload buildPushObject_all_alias_alert(String title,String content,String ...alias){
        return buildPushObject_all_audience_alert(title,content,Audience.alias(alias));
    }

    public static void setProduction(boolean pro){
        production = pro;
    }

    @Async
    public void sendPush(String title, String content, String... alias) {
        log.info("jpush 推送入参：title:{},content:{},alias:{}",title,content, Arrays.toString(alias));
        if (StringUtils.isBlank(content)||alias ==null||alias.length<=0)return;
        try {
            PushResult result = this.sendPushRetry(JPushClientWrapper.buildPushObject_all_alias_alert(title,content,alias),2);
            log.info("jpush 推送返回值:{}",result);
        } catch (APIRequestException e) {
            /**
             *  JPush这个地方的异常很坑，哪有将异常从JPush的服务，抛到调用者的服务当中的
             *  这地方不仅仅是异常，这地方是返回的非成功发送的异常返回信息
             *  @see JPushErrorCode
             */
            log.error("参数异常",e);
            log.info("http请求状态码: " + e.getStatus());
            log.info("jpush返回的错误码: " + e.getErrorCode());
            log.info("jpush错误信息: " + e.getErrorMessage());
        } catch (APIConnectionException e) {
            log.error(e.getMessage(),e);
        }
    }



}
