package com.truring.common.JPush;

import cn.jiguang.common.ClientConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "jpush", ignoreUnknownFields = true)
public class JPushProperties {


    /**
     * 应用id
     */
    private String appKey;

    /**
     * 应用密钥
     */
    private String masterSecret;


    /**
     * 该字段仅对 iOS 的 Notification 有效，如果不指定则为推送生产环境。注意：JPush 服务端 SDK 默认设置为推送 “开发环境”。
     * true：表示推送生产环境。
     * false：表示推送开发环境。
     */
    private boolean production;

    /**
     * jupsh连接超时时重试次数
     */
    private int maxRetryTimes = ClientConfig.DEFULT_MAX_RETRY_TIMES;



}
