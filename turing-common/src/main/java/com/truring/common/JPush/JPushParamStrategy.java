package com.truring.common.JPush;

/**
 * 参数解析策略
 */
public interface JPushParamStrategy {

    String[] audience(Object audience);

    String title(Object title);

    String content(Object content);


}
