package com.truring.common.templateEngine;

import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Map;

/**
 * @author ygl
 * @description 自定义
 * @date 2021/11/13 11:06
 */
@Slf4j
public class TuringFreemarkerTemplateEngine extends FreemarkerTemplateEngine {


    @Override
    protected void outputCustomFile(Map<String, String> customFile, TableInfo tableInfo, Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String otherPath = getPathInfo(OutputFile.other);
        customFile.forEach((key, value) -> {
            String fileName = String.format((otherPath + File.separator + entityName + "%s"), key);
            outputFile(new File(fileName), objectMap, templateFilePath(value));
        });
    }
}
