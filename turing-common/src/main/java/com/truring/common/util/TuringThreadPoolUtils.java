package com.truring.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.util.Assert;

import java.util.concurrent.*;

/**
 * spring自带两个线程池
 * ThreadPoolTaskExecutor 任务
 * ThreadPoolTaskScheduler 计划
 * @author ygl
 * @description 线程池工具
 * @date 2021/10/25 9:31
 */
@Slf4j
public class TuringThreadPoolUtils {


    public static ThreadPoolExecutor init(int corePoolSize,
                            int maximumPoolSize,
                            long keepAliveTime,
                            TimeUnit unit,
                            BlockingQueue<Runnable> workQueue,
                            String prefix
    ){
        CustomizableThreadFactory customizableThreadFactory = new CustomizableThreadFactory();
        customizableThreadFactory.setThreadNamePrefix(prefix);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize, maximumPoolSize, keepAliveTime, unit,
                workQueue,customizableThreadFactory);
        log.debug("****************************************************************init ThreadPoolUtils:{}****************************************************************",threadPoolExecutor);
        printParameter(threadPoolExecutor);
        return threadPoolExecutor;
    }


    public static void printParameter(ThreadPoolExecutor threadPoolExecutor){
        log.debug("ThreadPool parameter:PoolSize:{},MaxPoolSize:{},CorePoolSize:{},ActiveCount:{},KeepAliveSeconds:{}s"
                ,threadPoolExecutor.getPoolSize()
                ,threadPoolExecutor.getMaximumPoolSize()
                ,threadPoolExecutor.getCorePoolSize()
                ,threadPoolExecutor.getActiveCount()
                ,threadPoolExecutor.getKeepAliveTime(TimeUnit.SECONDS)
        );
    }



}
