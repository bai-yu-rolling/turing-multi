package com.truring.common.util;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.PropertyPlaceholderHelper;

import java.util.Properties;

/**
 * yml获取与解析
 * @author ygl
 */
public class YamlUtil {

    public static final PropertyPlaceholderHelper PROPERTY_PLACEHOLDER_HELPER = new PropertyPlaceholderHelper("${","}");

    /**
     * 获取classpath下的yaml文件
     * @param file
     * @return
     */
    public static Properties getYmlClasspathProperties(String file){
        ClassPathResource resource = new ClassPathResource(file);
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(resource);
        return yaml.getObject();
    }

    public static String parse(String str,Properties properties ){
        return  PROPERTY_PLACEHOLDER_HELPER.replacePlaceholders(str,properties);
    }

}
