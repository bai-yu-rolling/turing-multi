package com.truring.common.util;

import org.apache.commons.lang3.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * 时间，更多是用来学习，可以使用
 * https://hutool.cn/docs/index.html#/core/%E6%97%A5%E6%9C%9F%E6%97%B6%E9%97%B4/%E6%A6%82%E8%BF%B0
 * @author ygl
 */
public class TimeUtil {

    public static final String BASE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 转成普通时间
     * @param localDateTime
     * @return
     */
    public static Date toDate(LocalDateTime localDateTime){
        return Date.from(localDateTime.atZone( ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalDate localDate){
        return Date.from(localDate.atStartOfDay( ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate toLocalDate(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }


    /**
     * 获取今天开始时间
     * @return
     */
    public static LocalDateTime getDayStartTime() {
        //当天零点
        LocalDateTime todayStart = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);

        return todayStart;
    }

    /**
     * 获取今天结束时间
     * @return
     */
    public static LocalDateTime getDayEndTime() {
        //当天零点
        LocalDateTime todayEnd = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        return todayEnd;
    }

    /**
     *月第一天
     * @return
     */
    public static LocalDateTime getMonthStart(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime firstday = now.with(TemporalAdjusters.firstDayOfMonth()).withHour(0).withMinute(0).withSecond(0);

        return firstday;
    }

    public static LocalDateTime getMonthStart(int year,int month){
        LocalDateTime now = LocalDateTime.of(year,month,1,0,0);
        LocalDateTime firstday = now.with(TemporalAdjusters.firstDayOfMonth()).withHour(0).withMinute(0).withSecond(0);
        return firstday;
    }


    /**
     * 月最后一天
     * @return
     */
    public static LocalDateTime getMonthEnd(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastDay = now.with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59);
        return lastDay;
    }

    public static LocalDateTime getMonthEnd(int year,int month){
        LocalDateTime now = LocalDateTime.of(year,month,1,0,0);
        LocalDateTime lastDay = now.with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59);
        return lastDay;
    }

    /**
     *今年第一天
     * @return
     */
    public static LocalDateTime getYearStart(){
        LocalDateTime now = LocalDateTime.now();
        //某年的第一天
        LocalDateTime firstday = now.with(TemporalAdjusters.firstDayOfYear()).withHour(0).withMinute(0).withSecond(0);
        return firstday;

    }


    /**
     * 今年最后一天
     * @return
     */
    public static LocalDateTime getYearEnd(){
        LocalDateTime now = LocalDateTime.now();
        //某年的最后一天
        LocalDateTime lastDay = now.with(TemporalAdjusters.lastDayOfYear()).withHour(23).withMinute(59).withSecond(59);
        return lastDay;
    }


    /**
     * 季度开始时间
     * @param year
     * @param month
     * @return
     */
    public static LocalDateTime getQuarterStart(int year,int month){
        Month m = Month.of(month).firstMonthOfQuarter();
        return  LocalDateTime.of(LocalDate.of(year,m,1), LocalTime.MIN);
    }

    /**
     * 季度结束时间
     * @param year
     * @param month
     * @return
     */
    public static LocalDateTime getQuarterEnd(int year,int month){
        Month m = Month.of(month).firstMonthOfQuarter().plus(2L);
        return  LocalDateTime.of(LocalDate.of(year,m,1), LocalTime.MAX).with(TemporalAdjusters.lastDayOfMonth());
    }



    /**
     * 获取今天开始时间
     * @return
     */
    public static String getDayStartTime(String format) {
        //当天零点
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        return  getDayStartTime().format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * 获取今天结束时间
     * @return
     */
    public static String getDayEndTime(String format) {
        //当天零点
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        return getDayEndTime().format(DateTimeFormatter.ofPattern(format));
    }

    /**
     *月第一天
     * @return
     */
    public static String getMonthStart(String format){
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        return getMonthStart().format(DateTimeFormatter.ofPattern(format));

    }


    /**
     * 月最后一天
     * @return
     */
    public static String getMonthEnd(String format){
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        return getMonthEnd().format(DateTimeFormatter.ofPattern(format));
    }

    /**
     *今年第一天
     * @return
     */
    public static String getYearStart(String format){
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        //某年的第一天
        return getYearStart().format(DateTimeFormatter.ofPattern(format));

    }

    /**
     * 今年最后一天
     * @return
     */
    public static String getYearEnd(String format){
        //某年的最后一天
        if (StringUtils.isBlank(format))format = BASE_FORMAT;
        return getYearEnd().format(DateTimeFormatter.ofPattern(format));
    }





    /**
     *获取当前时间所在周的第一天
     * @return
     */
    public static LocalDate getMonday(){

        LocalDate now = LocalDate.now();
        LocalDate with = now.with(DayOfWeek.MONDAY);

        return with;
    }

    public static LocalDate getSunday(){

        LocalDate now = LocalDate.now();
        LocalDate with = now.with(DayOfWeek.SUNDAY);

        return with;
    }

    /**
     * 相差天数
     * @param startDate
     * @param endDate
     * @return
     */
    public static long localDateCompare(LocalDate startDate,LocalDate endDate){
        return startDate.until(endDate, ChronoUnit.DAYS);
    }

}
