package com.truring.common.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class DelayedDBWriter<T> {
    private final List<T> buffer;

    private final int bufferSize;

    private final WriteCallback<T> callback;


    private final ExecutorService executor;


    private final ReentrantLock lock;

    private final long timeoutInMillis;

    private long lastWriteTime;


    public interface WriteCallback<T> {
        void write(List<T> data) throws SQLException;
    }

    public DelayedDBWriter(int bufferSize, long timeoutInMillis, WriteCallback<T> callback) {
        this.buffer = Collections.synchronizedList(new LinkedList<>());
        this.bufferSize = bufferSize;
        this.callback = callback;
        this.lock = new ReentrantLock();;
        this.executor = Executors.newSingleThreadExecutor();
        this.timeoutInMillis = timeoutInMillis;
    }

    public void write(T data) {
        lock.lock();
        try {

            long currentTime = System.currentTimeMillis();
            buffer.add(data);
            if (buffer.size() == 1){
                lastWriteTime = currentTime;
            }
            if (currentTime - lastWriteTime > timeoutInMillis || buffer.size() >= bufferSize) {
                flush();
            }
        } finally {
            lock.unlock();
        }
    }

    public void flush() {
        lock.lock();
        try {
            if (buffer.isEmpty()) {
                return;
            }
            List<T> data = new ArrayList<>(buffer);
            buffer.clear();
            executor.execute(() -> {
                try {
                    callback.write(data);
                } catch (SQLException e) {
                    log.error("写入错误",e);
                }
            });
        } finally {
            lock.unlock();
        }
    }


    public void shutdown() {
        executor.shutdown();
    }

}