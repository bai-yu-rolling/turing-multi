package com.truring.common.config;


import cn.jiguang.common.ClientConfig;
import cn.jpush.api.JPushClient;
import com.truring.common.JPush.JPushClientWrapper;
import com.truring.common.JPush.JPushProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
@ConditionalOnMissingBean(JPushClientWrapper.class)
@EnableConfigurationProperties(JPushProperties.class)
@ConditionalOnProperty(prefix = "jpush", name = {"app-key","master-secret"})
public class JPushConfig {


    @Bean
    public JPushClientWrapper jPushClient(JPushProperties jpushProperties){
        log.debug("极光推送配置：{}",jpushProperties);
        JPushClientWrapper.setProduction(jpushProperties.isProduction());
        return new JPushClientWrapper(jPushClientBuild(jpushProperties));
    }


    public static JPushClient jPushClientBuild(JPushProperties jpushProperties){
        ClientConfig config = ClientConfig.getInstance();
        config.setMaxRetryTimes(jpushProperties.getMaxRetryTimes());
        return new JPushClient(jpushProperties.getMasterSecret(), jpushProperties.getAppKey(), null, config);
    }





}
