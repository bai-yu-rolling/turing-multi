package com.truring.common;

import cn.hutool.core.util.ClassUtil;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.AbstractTemplateEngine;
import com.truring.common.templateEngine.TuringFreemarkerTemplateEngine;
import com.truring.common.util.YamlUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Collections;
import java.util.Properties;

/**
 * https://mp.baomidou.com/guide/generator-new.html
 * @author ygl
 * @description 自动生成代码
 * @date 2021/9/28 8:05
 */
@Slf4j
public class GenerateCode {

    /**
     * 模板配置
     */
    public static final String CLASS_TEMP_PATH = "/temp";
    public static final String ENTITY_TEMP = CLASS_TEMP_PATH +"/entity.java";
    public static final String SERVICE_TEMP = CLASS_TEMP_PATH +"/service.java";
    public static final String SERVICE_IMPL_TEMP = CLASS_TEMP_PATH +"/serviceImpl.java";
    public static final String MAPPER_TEMP = CLASS_TEMP_PATH +"/mapper.java";
    public static final String MAPPER_XML_TEMP = CLASS_TEMP_PATH +"/mapper.xml";
    public static final String CONTROLLER_TEMP = CLASS_TEMP_PATH +"/controller.java";
    public static final String PAGE_PARAMS = CLASS_TEMP_PATH +"/param.java";
    public static final String MAIN = "/src/main";
    public static final String MAIN_JAVA = MAIN+"/java";
    public static final String MAIN_RESOURCES = MAIN+"/resources";

    public static String DATABASE_PROPERTY = "application-database.yml";
    public static String SQL_URL_PROPERTY = "spring.datasource.url";
    public static String SQL_USERNAME_PROPERTY = "spring.datasource.username";
    public static String SQL_PASSWORD_PROPERTY = "spring.datasource.password";
    public static String PROJECT_PATH = System.getProperty("user.dir");

    protected static AbstractTemplateEngine templateEngine = new TuringFreemarkerTemplateEngine();

    /**
     * 生成代码
     */
    public static void generate(GenerateConfig generateConfig){
        final String dir  =generateConfig.outputDir + MAIN_JAVA;
        if( !ClassUtil.isPrimitiveWrapper(generateConfig.primaryType)&&!String.class.equals(generateConfig.primaryType)){
            throw new UnsupportedOperationException("主键类型请用基本类的包装类或者String："+generateConfig.primaryType);
        }
        FastAutoGenerator.create(generateConfig.dataSource)
                // 全局配置
                .globalConfig((builder) -> builder
                                .author(generateConfig.author)
                                .disableOpenDir()
//                    .fileOverride()
                                .outputDir(dir)
                                .commentDate("yyyy-MM-dd HH:mm:ss")
                )
                // 包配置
                .packageConfig((builder) -> builder
                        .parent(generateConfig.parent)
                        .moduleName(generateConfig.module)
                        .entity(generateConfig.entity)
                        .controller(generateConfig.controller)
                        .service(generateConfig.service)
                        .serviceImpl(generateConfig.serviceImpl)
                        .mapper(generateConfig.mapper)
                        .other("params")
                        .pathInfo(Collections.singletonMap(OutputFile.xml, generateConfig.outputDir +MAIN_RESOURCES+ File.separator + "mapper"+File.separator+generateConfig.module))
                )
                //模板路径
                .templateConfig((builder) -> builder
                        .entity(ENTITY_TEMP)
                        .service(SERVICE_TEMP)
                        .serviceImpl(SERVICE_IMPL_TEMP)
                        .mapper(MAPPER_TEMP)
                        .xml(MAPPER_XML_TEMP)
                        .controller(CONTROLLER_TEMP)

                )
                // 策略配置
                .strategyConfig(builder -> builder.addTablePrefix(generateConfig.prefix))
                .strategyConfig((builder) -> builder
                        .addInclude(generateConfig.tableName)
//                        .enableSchema()
                        .entityBuilder()
                        .enableLombok()
                ) .strategyConfig((builder) -> builder
                        .controllerBuilder()
                        .enableRestStyle()
                ).strategyConfig((builder) -> builder
                        .mapperBuilder()
                        .enableMapperAnnotation()
                        .enableBaseResultMap()
                        .enableBaseColumnList()
                ).strategyConfig(builder -> builder
                        .serviceBuilder()
                        .formatServiceFileName("%sService")
                        .formatServiceImplFileName("%sServiceImp")
                )
                //注入
                .injectionConfig(builder -> builder
                        .customMap(Collections.singletonMap("primaryType", generateConfig.primaryType.getSimpleName()))
                        .customFile(Collections.singletonMap("Param.java",PAGE_PARAMS))
                )
                //模板引擎配置，默认 Velocity 可选模板引擎 Beetl 或 Freemarker
//                .templateEngine(new BeetlTemplateEngine())
                .templateEngine(templateEngine)

                .execute();
    }

    private static class GenerateConfig {


        /**
         * 数据源
         */
        private DataSourceConfig.Builder dataSource;
        /**
         * 输出到那个文件
         */
        private String outputDir=GenerateCode.PROJECT_PATH;
        /**
         * 基本包名例如com.turing
         */
        private String parent="com.turing";
        /**
         * 模块名称
         */
        private String module="mode";
        /**
         * 表名
         */
        private String tableName;

        /**
         * controller包名
         */
        private String controller = "controller";
        /**
         * entity包名
         */
        private String entity = "models";
        /**
         * service包名
         */
        private String service = "service";
        /**
         * serviceImpl包名
         */
        private String serviceImpl = "service.impl";
        /**
         * mapper包名
         */
        private String mapper = "mapper";

        /**
         * 作者
         */
        private String author;
        /**
         * id类型
         */
        private Class<?> primaryType = Long.class;
        /**
         * 要删除的前缀
         */
        private String prefix = "";

    }

    public static class Builder  {

        private GenerateConfig generateConfig;

        public static Builder builderFactory(){
            return new Builder();
        }
        public static Builder builderFactory(String url, String username, String password,String schema){
            return new Builder(url, username, password,schema);
        }

        public static Builder builderFactory(String url, String username, String password){
            return new Builder(url, username, password);
        }

        public Builder(String url, String username, String password) {
            this(url,username,password,null);
        }

        public Builder(String url, String username, String password,String schema) {
            this.generateConfig = new GenerateConfig();
            generateConfig.dataSource = new DataSourceConfig
                    .Builder(url, username, password).schema(schema);
            log.debug("url:{}\n username:{}",url,username);
        }

        /**
         * classPath下spring的yum配置文件
         * @param yum
         * @param urlKey
         * @param usernameKey
         * @param passwordKey
         */
        public Builder(String yum,String urlKey,String usernameKey,String passwordKey,String schema){
            this.generateConfig = new GenerateConfig();
            Properties properties = YamlUtil.getYmlClasspathProperties(yum);
            String url = properties.getProperty(urlKey);
            String username = properties.getProperty(usernameKey);
            String password = properties.getProperty(passwordKey);
            url = YamlUtil.parse(url,properties);
            username = YamlUtil.parse(username,properties);
            password = YamlUtil.parse(password,properties);
            generateConfig.dataSource = new DataSourceConfig.Builder(url, username, password).schema(schema);
            log.debug("url:{}\n username:{}",url,username);

        }

        public Builder(){
           this(GenerateCode.DATABASE_PROPERTY,GenerateCode.SQL_URL_PROPERTY,GenerateCode.SQL_USERNAME_PROPERTY,GenerateCode.SQL_PASSWORD_PROPERTY,"public");
        }

        public Builder setOutputDir(String outputDir) {
            generateConfig.outputDir = outputDir;
            return this;
        }

        public Builder setParent(String parent) {
            generateConfig.parent = parent;
            return this;
        }

        public Builder setModule(String module) {
            generateConfig.module = module;
            return this;
        }

        public Builder setTableName(String tableName) {
            generateConfig.tableName = tableName;
            return this;
        }


        public Builder setAuthor(String author) {
            generateConfig.author = author;
            return this;
        }

        public Builder setPrimaryType(Class<?> primaryType) {
            generateConfig.primaryType = primaryType;
            return this;
        }

        public Builder setPrefix(String prefix) {
            generateConfig.prefix = prefix;
            return this;
        }

        public Builder setController(String controller) {
            generateConfig.controller = controller;
            return this;
        }

        public Builder setEntity(String entity) {
            generateConfig.entity = entity;
            return this;
        }

        public Builder setService(String service) {
            generateConfig.service = service;
            return this;
        }

        public Builder setServiceImpl(String serviceImpl) {
            generateConfig.serviceImpl = serviceImpl;
            return this;
        }

        public Builder setMapper(String mapper) {
            generateConfig.mapper = mapper;
            return this;
        }

        public GenerateConfig build() {
            return generateConfig;
        }

        public void execute(){
            GenerateCode.generate(generateConfig);
        }
    }
}
