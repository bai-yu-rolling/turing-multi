package com.truring.common.thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 多线程事务
 * @author ygl
 */
@Slf4j
public class BatchTransaction<T>{

    /**
     * 异常回调接口
     * @author ygl
     */
    public interface ExceptionCall<T> {
        /**
         * 异常捕获回调
         * @param e
         */
        void exceptionHandler(Exception e);


    }


    /**
     *任务队列
     */
    private final List<Callable<T>> tasks = new LinkedList<>();


    /**
     * 异常回调
     */
    private ExceptionCall<T> resultCall = e -> {};

    /**
     * 事务管理
     */
    private PlatformTransactionManager transactionManager;

    /**
     * 线程池
     */
    private final ExecutorService threadPoolExecutor;

    /**
     * 是否开启事务
     */
    private boolean hasTransaction = true;

    /**
     * 提交状态
     */
    private volatile boolean commitOrNot = true;

    /**
     * 计数
     */
    private final AtomicInteger count = new AtomicInteger(0);

    /**
     * allOf返回值
     */
    private CompletableFuture<Void> allFuture;

    /**
     * 结果队列
     */
    private final ConcurrentLinkedQueue<T> resultQueue = new ConcurrentLinkedQueue<T>();

    private static final DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();


    public BatchTransaction(ExecutorService threadPoolExecutor,PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    /**
     * 非事务
     */
    public BatchTransaction(ExecutorService threadPoolExecutor) {
        this.hasTransaction = false;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    private T handleTask(Callable<T> sourceTask, CountDownLatch countDownLatch) {
        TransactionStatus txStatus = null;
        T result = null;
        try {
            if (!commitOrNot) return null;
            txStatus = transactionManager.getTransaction(transactionDefinition);
            result = sourceTask.call();
            resultQueue.add(result);
            count.addAndGet(1);
        } catch (Exception e) {
            log.error("执行异常", e);
            commitOrNot = false;
            resultCall.exceptionHandler(e);
        } finally {
            countDownLatch.countDown();
        }
        if (txStatus == null) return null;
        try {
            log.debug("\n==== [wait other transaction]");
            countDownLatch.await();
        } catch (InterruptedException e) {
            commitOrNot = false;
            log.error("中断异常", e);
            resultCall.exceptionHandler(e);
        }
        // commit or rollback?
        if (commitOrNot) {
            log.debug("\n==== [commit]");
            transactionManager.commit(txStatus);
        } else {
            log.debug("\n==== [rollback]");
            transactionManager.rollback(txStatus);
        }
        return result;
    }

    private T handleTaskNoTransaction(Callable<T> sourceTask) {
        if (!commitOrNot) return null;
        T result = null;
        try {
            result = sourceTask.call();
            resultQueue.add(result);
            count.addAndGet(1);
        } catch (Exception e) {
            log.error("执行异常", e);
            resultCall.exceptionHandler(e);
        }
        return result;
    }

    public void execute(){

        CountDownLatch latch = new CountDownLatch(tasks.size());

        allFuture = CompletableFuture.allOf(
                tasks.stream().map(
                        sourceTask -> CompletableFuture.supplyAsync(
                                () -> hasTransaction?handleTask(sourceTask, latch):handleTaskNoTransaction(sourceTask)
                                ,threadPoolExecutor
                        )
                ).toArray(CompletableFuture[]::new)
        );

    }

    private void join(){
        if (allFuture == null) throw new IllegalStateException("任务未执行");
        if (allFuture.isDone())return;
        log.debug("\n==== [wait all]");
        allFuture.join();
    }

    public BatchTransaction<T> add(Callable<T> task){
        tasks.add(task);
        return this;
    }

    public BatchTransaction<T> exception(ExceptionCall<T> call){
        resultCall = call;
        return this;
    }



    public ConcurrentLinkedQueue<T> getResultQueue() {
        join();
        return resultQueue;
    }

    public int getCount() {
        join();
        return count.get();
    }

    public boolean getCommitOrNot() {
        join();
        return commitOrNot;
    }
}