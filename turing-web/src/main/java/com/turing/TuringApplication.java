package com.turing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author ygl
 */
@SpringBootApplication(exclude = {})
@ServletComponentScan
@EnableCaching
@EnableAsync
public class TuringApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(TuringApplication.class, args);
//        applicationContext.publishEvent(e);
    }



}
