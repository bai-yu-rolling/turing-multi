package com.turing.system.quartz;

import org.quartz.*;
import org.quartz.spi.MutableTrigger;

import java.text.ParseException;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.TimeZone;

public class CronPointScheduleBuilder extends ScheduleBuilder<CronPointTriggerImpl> {

    private LocalTime[] times;

    private CronExpression cronExpression;
    private int misfireInstruction = CronTrigger.MISFIRE_INSTRUCTION_SMART_POLICY;

    protected CronPointScheduleBuilder(CronExpression cronExpression,LocalTime []times) {
        this.cronExpression = cronExpression;
        this.times = times;
    }

    public static CronPointScheduleBuilder cronPointSchedule(String cronExpression,LocalTime [] times) {
        try {
            return new CronPointScheduleBuilder(new CronExpression(cronExpression),times);
        } catch (ParseException | IllegalArgumentException e) {
            // all methods of construction ensure the expression is valid by
            // this point...
            throw new RuntimeException("参数异常：cronExpression"+cronExpression+";times："+ Arrays.toString(times),e);
        }
    }

    @Override
    public MutableTrigger build() {

        CronPointTriggerImpl ct = new CronPointTriggerImpl(times);
        //父类未开放，此处需要单独处理子类
        ct.setCronExpression(cronExpression);
        ct.setZoneId(cronExpression.getTimeZone());
        ct.setMisfireInstruction(misfireInstruction);

        return ct;
    }

    public CronPointScheduleBuilder inTimeZone(TimeZone timezone) {
        cronExpression.setTimeZone(timezone);
        return this;
    }


    public CronPointScheduleBuilder withMisfireHandlingInstructionIgnoreMisfires() {
        misfireInstruction = Trigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY;
        return this;
    }

    public CronPointScheduleBuilder withMisfireHandlingInstructionDoNothing() {
        misfireInstruction = CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING;
        return this;
    }


    public CronPointScheduleBuilder withMisfireHandlingInstructionFireAndProceed() {
        misfireInstruction = CronTrigger.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW;
        return this;
    }
}
