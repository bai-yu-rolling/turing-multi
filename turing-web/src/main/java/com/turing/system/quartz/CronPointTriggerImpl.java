package com.turing.system.quartz;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.quartz.impl.triggers.CronTriggerImpl;

import java.io.Serializable;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Slf4j
public final class CronPointTriggerImpl extends CronTriggerImpl {

    private final static long serialVersionUID =  1l;

    /**
     * 带头节点的双向循环链表，将其抽象为时钟表盘
     */
    private final TimePoint clockHeard = new TimePoint(null);

    private CronExpression localCronExpression;


    private ZoneId zoneId = ZoneId.systemDefault();


    public CronPointTriggerImpl(LocalTime[] timePoits) {
        if (timePoits == null || timePoits.length <= 0)
            throw new IllegalArgumentException("timePoint不能为空");
        Arrays.sort(timePoits);
        Set<TimePoint> dist = new HashSet<>();
        for (LocalTime p: timePoits){
            TimePoint t = addPoint(p);
            if (!dist.add(t)){
                throw new IllegalArgumentException("时间点不能相同，精确到秒");
            }
        }
    }

    public TimePoint getClockHeard() {
        return clockHeard;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(TimeZone timeZone) {
        this.zoneId = timeZone.toZoneId();
        super.setTimeZone(timeZone);
    }

    public CronExpression getLocalCronExpression() {
        return localCronExpression;
    }

    @Override
    public void setCronExpression(String cronExpression) throws ParseException {
        this.setCronExpression(new CronExpression(cronExpression));
    }

    @Override
    public void setCronExpression(CronExpression localCronExpression) {
        String ex = localCronExpression.getCronExpression();
        if (StringUtils.isBlank(ex))return;
        String[] exArray = ex.split(" ");
        if (exArray.length > 3){
            exArray = Arrays.copyOfRange(exArray,3,exArray.length);
            //cron的时间固定位一天的开始，用于比较
            ex = String.format("0 0 0 %s",String.join(" ",exArray));
        }

        try {
            this.localCronExpression = new CronExpression(ex);
            super.setCronExpression(localCronExpression);
        } catch (ParseException e) {
            throw new RuntimeException("无效的cron表达式："+ex);
        }
    }

    /**
     * 这个方法的意义是，传入一个时间，计算这个时间之后要执行的时间
     * @param afterTime
     * @return
     */
    @Override
    protected Date getTimeAfter(Date afterTime) {
        if (localCronExpression == null) return null;
        LocalDateTime afterLocalDateTime = afterTime.toInstant().atZone(zoneId).toLocalDateTime();
        log.debug("*************************传入的时间{}**************************************",afterLocalDateTime);


        TimePoint nextPoint = clockHeard.nextPoint;

        LocalDate nextLocalDate = afterLocalDateTime.toLocalDate();

        //cron的时间固定位一天的开始，找到当前传入日期的下一次执行日期
        afterTime = Date.from( nextLocalDate.atStartOfDay().atZone(zoneId).toInstant());
        if (!localCronExpression.isSatisfiedBy(afterTime)){
            //不是当天找下一次运行时间
            afterTime = super.getTimeAfter(afterTime);
        }else {
            //时间点循环，找到传入时间点的下一次执行时间点
            nextPoint = nextClockPoint(afterLocalDateTime.toLocalTime());
        }

        //当下一次时间点与头结点相遇时，说明这一天过完了
        if (nextPoint == clockHeard){
            nextPoint = nextPoint.nextPoint;
            //找到下一执行日期
            afterTime = super.getTimeAfter(afterTime);
        }

        if (afterTime == null) {
            log.debug("执行时间在当前时间之前或者最后一次执行，上级代码会处理此处的空问题");
            return null;
        };

        nextLocalDate = afterTime.toInstant().atZone(zoneId).toLocalDate();
        log.debug("*************************下次执行的时间点{}**************************************",nextPoint);
        log.debug("*************************下次执行的日期{}**************************************",nextLocalDate);
        LocalDateTime result = LocalDateTime.of(nextLocalDate,nextPoint.time);
        log.debug("*************************下次执行时间{}**************************************",result);
        return Date.from( result.atZone(zoneId).toInstant());
    }

    @Override
    protected Date getTimeBefore(Date eTime) {
        return null;
    }


    /**
     * 这个是必须为true，当为true是，这个类将被作为扩展类处理，放到数据库中的qrtz_blob_triggers，
     * 不然将会放到qrtz_cron_triggers。
     * true每次执行时会生成当前对象的实例，false会生成其父类的对象，这样就无法执行扩展的方法
     *
     * @return
     */
    @Override
    public boolean hasAdditionalProperties() {
        return true;
    }

    static class TimePoint implements Serializable {
        public static final long serialVersionUID = -1L;
        LocalTime time;
        TimePoint nextPoint;
        TimePoint prePoint;

        public TimePoint(LocalTime time) {
            this.time = time;
            this.nextPoint = this;
            this.prePoint = this;
        }

        @Override
        public String toString() {
            return "TimePoint{" +
                    "time=" + time +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TimePoint point = (TimePoint) o;
            return Objects.equals(time, point.time);
        }

        @Override
        public int hashCode() {
            return Objects.hash(time);
        }
    }

    private TimePoint addPoint(LocalTime val) {
        TimePoint node = new TimePoint(val);
        TimePoint end = clockHeard.prePoint;
        end.nextPoint = node;
        node.nextPoint = clockHeard;
        node.prePoint = end;
        clockHeard.prePoint = node;
        return node;
    }

    private TimePoint nextClockPoint(LocalTime appoint){
        TimePoint nextPoint = clockHeard;
        LocalTime afterLocalTime = appoint;
        while (nextPoint.nextPoint != clockHeard && nextPoint.nextPoint.time.until(afterLocalTime,ChronoUnit.SECONDS) >= 0){
            nextPoint = nextPoint.nextPoint;
        }
        return nextPoint.nextPoint;
    }



}