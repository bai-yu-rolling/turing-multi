package com.turing.system.params;

import com.turing.core.PageParam;

import lombok.Getter;
import lombok.Setter;
/**
* <p>
*  查询参数
* </p>
*
* @author ygl
* @since 2022-09-27 17:09:05
*/
@Getter
@Setter
public class UserParam extends PageParam<UserParam> {

}
