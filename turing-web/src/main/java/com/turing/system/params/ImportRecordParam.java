package com.turing.system.params;


import com.turing.core.PageParam;
import lombok.Getter;
import lombok.Setter;

/**
* <p>
*  查询参数
* </p>
*
* @author ygl
* @since 2022-08-05 17:00:53
*/
@Getter
@Setter
public class ImportRecordParam extends PageParam<ImportRecordParam> {


    private String title;
}
