package com.turing.system.mapper;

import com.turing.system.models.User;
import com.turing.system.params.UserParam;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ygl
 * @since 2022-09-27 17:09:05
 */
@Mapper
public interface UserMapper{


    /**
     * 新增
     * @param user
     * @return 个数
     */
    int insert(User user);

    /**
     * 批量新增
     * @param userList
     * @return 个数
     */
    int insertBatch(@Param("list") List<User> userList);

    /**
     * 删除
     * @param id
     * @return 个数
     */
    int deleteById(@Param("id") Long id);

    /**
     * 批量删除
     * @param ids
     * @return 个数
     */
    int deleteBatchId(@Param("ids") List<Long> ids);

    /**
     * 更新
     * @param user
     * @return 个数
     */
    int update(User user);

    /**
     * 根据id查询
     * @param id
     * @return User
     */
    User selectById(@Param("id") Long id);


    /**
    * 参数查询
    * @param params
    * @return
    */
    List<User> selectByParams(UserParam params);

}
