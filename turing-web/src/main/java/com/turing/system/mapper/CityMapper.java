package com.turing.system.mapper;

import com.turing.system.models.City;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ygl
 * @date 2021/8/19 10:41
 */
@Mapper
public interface CityMapper {


    /**
     * 查询全部
     * @return city
     */
    List<City> selectAll();

    /**
     * 查询直系
     * @param pcd
     * @return city
     */
    List<City> selectByParentCode(String pcd);


    List<Map<String,Object>> test();
    List<Map<String,Object>> test2();

    int testUpdate(String username);

}
