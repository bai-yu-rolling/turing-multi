package com.turing.system.mapper;

import com.turing.system.models.ImportRecord;
import com.turing.system.params.ImportRecordParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ygl
 * @since 2022-08-05 17:00:53
 */
@Mapper
public interface ImportRecordMapper {


    /**
     * 新增
     * @param importRecord
     * @return 个数
     */
    int insert(ImportRecord importRecord);

    /**
     * 批量新增
     * @param importRecordList
     * @return 个数
     */
    int insertBatch(@Param("list") List<ImportRecord> importRecordList);

    /**
     * 删除
     * @param id
     * @return 个数
     */
    int deleteById(@Param("id") Long id);

    /**
     * 批量删除
     * @param ids
     * @return 个数
     */
    int deleteBatchId(@Param("ids") List<Long> ids);

    /**
     * 更新
     * @param importRecord
     * @return 个数
     */
    int update(ImportRecord importRecord);

    int temporaryStorageEnd(@Param("id") Long id);

    int addCount(@Param("id") Long id,@Param("count") int count);

    /**
     * 根据id查询
     * @param id
     * @return ImportRecord
     */
    ImportRecord selectById(@Param("id") Long id);


    /**
    * 参数查询
    * @param params
    * @return
    */
    List<ImportRecord> selectByParams(ImportRecordParam params);

}
