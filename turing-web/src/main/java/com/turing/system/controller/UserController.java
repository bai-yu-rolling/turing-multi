package com.turing.system.controller;


import cn.hutool.core.util.IdUtil;
import com.github.pagehelper.PageInfo;
import com.truring.common.thread.BatchTransaction;
import com.truring.common.util.TuringThreadPoolUtils;
import com.turing.core.PageResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.bind.annotation.RequestMapping;
import com.turing.system.service.UserService;
import com.turing.system.models.User;
import com.turing.system.params.UserParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import com.turing.core.Result;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ygl
 * @since 2022-09-27 17:09:05
 */
@Slf4j
@RestController
@RequestMapping("/system/user")
public class UserController {


    private final PlatformTransactionManager platformTransactionManager;
    private final UserService  userService;

    public UserController(PlatformTransactionManager platformTransactionManager, UserService userService) {
        this.platformTransactionManager = platformTransactionManager;
        this.userService = userService;
    }

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody User user){
        return Result.success(userService.save(user));
    }

    @PutMapping("/update")
    public Result<Integer> update(@RequestBody User user){
        return Result.success(userService.update(user));
    }

    @DeleteMapping("/delete/{id}")
    public Result<Integer> delete(@PathVariable Long id){
        return Result.success(userService.deleteById(id));
    }

    @GetMapping("/get/{id}")
    public Result<User> getById(@PathVariable Long id){
        return Result.success(userService.getById(id));
    }

     @GetMapping("/list")
     public PageResult list(UserParam param){
        PageInfo pageInfo = userService.list(param);
        return PageResult.success(pageInfo);
     }

    private static final ThreadPoolExecutor executorService = TuringThreadPoolUtils.init( 10, Integer.MAX_VALUE, 10, TimeUnit.MINUTES, new SynchronousQueue<>(), "batch-");;
    @GetMapping("test5")
    public int test5(int ei,int len){

        BatchTransaction<Integer> bt = new BatchTransaction<Integer>(executorService,platformTransactionManager);

        for (int i = 0; i < len; i++) {
            int finalI = i;
            bt.add(()->{
                if (finalI == ei)throw new Exception("错误");
                User u = new User();
                u.setId(IdUtil.getSnowflakeNextId());
                u.setUsername("yang"+finalI);
                u.setPassword("123");
                userService.save(u);
                return finalI;
            });
        }
        bt.execute();
        log.info("异步执行");
        if (bt.getCommitOrNot()){
            return bt.getCount();
        }
        return 0;
    }

}
