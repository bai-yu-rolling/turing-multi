package com.turing.system.controller;

import com.turing.system.models.City;
import com.turing.system.models.Tr;
import com.turing.system.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ygl
 * @date 2021/9/23
 */
@Slf4j
@RestController
@RequestMapping("/system/city")
public class CityController {


    private final CityService cityService;
    private final StringRedisTemplate stringRedisTemplate;

    public CityController(CityService cityService, StringRedisTemplate stringRedisTemplate) {
        this.cityService = cityService;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Autowired
    private CacheManager cacheManager;


    @GetMapping("all")
    public List<City> getAll(){
        log.info("*****************************************************************");
        Cache c = cacheManager.getCache("city");
        return cityService.getAllForTree();
    }

    @PutMapping("refresh")
    public void refreshAll() {
        cityService.refreshCache();
    }

    @GetMapping("/{pcd}")
    public List<City>  getByParentCode(@PathVariable String pcd){
        int a = 1/0;
       return cityService.getCityByPCD(pcd);
    }


    @GetMapping("test3")
    public ResponseEntity getTest3(@RequestParam String id){

        return ResponseEntity.ok(stringRedisTemplate.opsForSet().getOperations().boundGeoOps("key0"));
    }
    @GetMapping("test4")
    public ResponseEntity getTest4(){
        log.info("{}","11111111111111111");
        cityService.test();
        return null;
    }


}