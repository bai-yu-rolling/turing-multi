package com.turing.system.controller;


import com.github.pagehelper.PageInfo;
import com.turing.core.PageResult;
import com.turing.core.Result;
import com.turing.system.models.ImportRecord;
import com.turing.system.models.ImportRecordWrapper;
import com.turing.system.models.MainTableAware;
import com.turing.system.params.ImportRecordParam;
import com.turing.system.service.AsyncExcelService;
import com.turing.system.service.ImportRecordService;
import com.turing.system.service.ImportTransferStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ygl
 * @since 2022-08-05 17:00:53
 */
@RestController
@RequestMapping("/sys/import-record")
@Slf4j
public class ImportRecordController {


    private final ImportRecordService importRecordService;
    private final AsyncExcelService asyncExcelService;
    private final static Map<String, ImportTransferStrategy<? extends MainTableAware>> STRATEGY_CACHE = new HashMap<>(64);

    private final static List<Map<String,String>> tables = new LinkedList<>();

    public ImportRecordController(ImportRecordService importRecordService, AsyncExcelService asyncExcelService, List<ImportTransferStrategy<? extends  MainTableAware>> importTransfers) {
        this.importRecordService = importRecordService;
        this.asyncExcelService = asyncExcelService;
        for (ImportTransferStrategy<? extends  MainTableAware> s: importTransfers){
            STRATEGY_CACHE.put(s.getTempTableName(),s);
            Map<String,String> o = new HashMap<>();
            o.put("name",s.name());
            o.put("value",s.getTempTableName());
            tables.add(o);
        }
    }

    @GetMapping("/tables")
    public Result<?> tables(){
        return Result.success(tables);
    }

    @GetMapping("/list")
    public PageResult<?> list(ImportRecordParam param){
        PageInfo<ImportRecord> pageInfo = importRecordService.list(param);
        return PageResult.success(pageInfo);
    }


    @PostMapping("/import")
    public Result<?> importFile(@RequestPart MultipartFile file,String title,Integer type){
        try {
            ImportTransferStrategy<? extends  MainTableAware> importTransfer = STRATEGY_CACHE.get("table_name");
//            LoginAppUser loginAppUser = SysUserUtil.getLoginAppUser();
            ImportRecordWrapper wrapper = new ImportRecordWrapper();
            wrapper.setTableName(importTransfer.getTableName())
                    .setTempTableName(importTransfer.getTempTableName())
                    .setRecordIdColumn(importTransfer.getRecordIdColumn())
                    .setTitle(title)
                    .setType(type);
//                    .setCreateUserId(loginAppUser.getUserId())
//                    .setCreateUserName(loginAppUser.getName());
            asyncExcelService.asyncImport(file.getInputStream(),wrapper,importTransfer);
            return Result.success(null,"正在导入临时表中，请刷新查看导入进度");
        } catch (IOException e) {
            log.error("网路异常",e);
            return Result.failed("网路异常");
        }
    }

    @PostMapping("/transfer")
    public Result<?> transfer(Long batchId){
        ImportTransferStrategy<? extends  MainTableAware> importTransfer = STRATEGY_CACHE.get("table_name");
        asyncExcelService.transfer(batchId,importTransfer);
        return Result.success(null,"导入成功");
    }


    @PostMapping("/nullify")
    public Result<?> nullify(Long batchId){
        asyncExcelService.nullify(batchId);
        return Result.success(null,"作废成功");
    }



}
