package com.turing.system.models;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Data
public class ImportRecordWrapper {

    @NotNull(message = "导入的表名不能为空")
    private List<String> tableName;

    @NotBlank(message = "临时表名不能为空")
    private String tempTableName;

    @NotBlank(message = "标题不能为空")
    private String title;

    @NotBlank(message = "创建人id不能为空")
    private String createUserId;

    @NotBlank(message = "创建人名字不能为空")
    private String createUserName;

    @NotNull(message = "类型不能为空")
    @Max(ImportRecord.FORMAL_TYPE)
    @Min(ImportRecord.FORMAL_TYPE)
    private Integer type;

    private String tableNameSplicing;

    @NotNull(message = "临时表中主表关联id的字段名称不能为空")
    private String recordIdColumn;



    public ImportRecordWrapper setTableName(List<String> tableName) {
        this.tableName = tableName;
        return this;
    }

    public ImportRecordWrapper setTableName(String ...tableName) {
        this.tableName = Arrays.stream(tableName).collect(Collectors.toList());
        return this;
    }

    public String getTableNameSplicing() {
        if (tableName == null) return null;
        return String.join(",", tableName);
    }

    public ImportRecordWrapper setTempTableName(String tempTableName) {
        this.tempTableName = tempTableName;
        return this;
    }

    public ImportRecordWrapper setTitle(String title) {
        this.title = title;
        return this;
    }

    public ImportRecordWrapper setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public ImportRecordWrapper setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
        return this;
    }

    public ImportRecordWrapper setType(Integer type) {
        this.type = type;
        return this;
    }

    public ImportRecordWrapper setTableNameSplicing(String tableNameSplicing) {
        this.tableNameSplicing = tableNameSplicing;
        return this;
    }

    public ImportRecordWrapper setRecordIdColumn(String recordIdColumn) {
        this.recordIdColumn = recordIdColumn;
        return this;
    }

}
