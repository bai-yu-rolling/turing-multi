package com.turing.system.models;

import cn.hutool.core.util.IdUtil;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author ygl
 * @since 2022-08-05 17:00:53
 */
@Getter
@Setter
public class ImportRecord implements Serializable {


    private static final long serialVersionUID = 1L;

    public static final int NULLIFY = 0;
    //解析
    public static final int PARSING = 1;
    //解析失败
    public static final int PARSED_ERROR = 1 << 1;
    //临时存储
    public static final int TEMPORARY_STORAGE = 1 << 2;
    //临时存储失败
    public static final int TEMPORARY_STORAGE_ERROR = 1 << 3;
    //正式存储
    public static final int FORMAL_STORAGE = 1 << 4;
    //正式存储失败
    public static final int FORMAL_STORAGE_ERROR = 1 << 5;
    //临时存储删除
    public static final int TEMPORARY_STORAGE_DELETE = 1 << 6;
    //导入中
    public static final int IMPORTING = 1 << 7;
    //对比中
    public static final int COMPARING = 1 << 8;
    //已过滤
    public static final int COMPARED = 1 << 9;
    //对比异常
    public static final int COMPARED_ERROR = 1 << 10;


    /**
     * 正常导入数据
     */
    public static final int FORMAL_TYPE = 0;
    /**
     *用于矫正的数据
     */
//    public static final int CORRECT_TYPE = 1;
    /**
     * 对比结果
     */
//    public static final int CORRECT_RESULT = 2;

    private Long id;

    /**
     * 实际数量
     */
    private Integer count;

    /**
     * 导入状态
     */
    private Integer status;

    private String tableName;

    private String tempTableName;

    /**
     * 执行信息
     */
    private String message;

    /**
     * 导入数量
     */
    private Integer importCount;

    /**
     * 比较结果的数量
     */
    private Integer compareResultCount;

    /**
     * 转移时间（加入到正式表中的时间）
     */
    private Date transferTime;

    /**
     * 导入类型（对比数据或者是正式数据）
     */
    private Integer type;

    /**
     * 标题，方便查找
     */
    private String title;

    private String createUserId;

    private String createUserName;

    private Date createTime;

    private String recordIdColumn;

    private Integer version;


    public ImportRecord() {
        this.id = IdUtil.getSnowflakeNextId();
    }

    public ImportRecord(ImportRecordWrapper wrapper) {
        this.id = IdUtil.getSnowflakeNextId();
        this.title = wrapper.getTitle();
        this.createUserId = wrapper.getCreateUserId();
        this.createUserName = wrapper.getCreateUserName();
        this.tableName = wrapper.getTableNameSplicing();
        this.tempTableName = wrapper.getTempTableName();
        this.type = wrapper.getType();
        this.recordIdColumn = wrapper.getRecordIdColumn();
        this.status = PARSING;
    }

    public static ImportRecord builder(){
        return new ImportRecord();
    }


    public ImportRecord setCompareResultCount(Integer compareResultCount) {
        this.compareResultCount = compareResultCount;
        return this;
    }

    public ImportRecord setId(Long id) {
        this.id = id;
        return this;
    }

    public ImportRecord setCount(Integer count){
        this.count = count;
        return this;
    }

    public ImportRecord setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public ImportRecord setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public ImportRecord setTempTableName(String tempTableName) {
        this.tempTableName = tempTableName;
        return this;
    }

    public ImportRecord setMessage(String message) {
        this.message = message;
        return this;
    }

    public ImportRecord setImportCount(Integer importCount) {
        this.importCount = importCount;
        return this;
    }

    public ImportRecord setType(Integer type) {
        this.type = type;
        return this;
    }

    public ImportRecord setTitle(String title) {
        this.title = title;
        return this;
    }

    public ImportRecord setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public ImportRecord setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
        return this;
    }

    public ImportRecord setRecordIdColumn(String recordIdColumn) {
        this.recordIdColumn = recordIdColumn;
        return this;
    }

    public ImportRecord setTransferTime(Date transferTime) {
        this.transferTime = transferTime;
        return this;
    }

    public ImportRecord setVersion(Integer version) {
        this.version = version;
        return this;
    }
}
