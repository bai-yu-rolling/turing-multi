package com.turing.system.models;

import cn.hutool.core.io.file.FileAppender;
import cn.hutool.core.util.IdUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ygl
 * @date 2021/8/19 10:30
 */
@Data
public class City implements Serializable ,Cloneable{


   private String parentCode;
   private String areaName;
   private int level;
   private String areaCode;

   List<City> cities;

   public City() {
   }

   public City(String parentCode) {
      this.parentCode = parentCode;
   }

   @Override
   public City clone(){
      try {
         return (City) super.clone();
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
      }
      return  null;
   }

   public void print(Long pid, FileAppender appender){
      Long id = IdUtil.getSnowflakeNextId();
      appender.append(String.format(  "INSERT INTO HSJC.SYS_AREA(ID, AREACODE, AREANAME, SHORTNAME, AREALEVEL, PARENTCODE, PARENTID, ADDRESS, ZIPCODE, EMAIL, REMARK, ISENABLE, CREATEUSERNAME, CREATEUSER, CREATEDATE, LASTUPDATEUSERNAME, LASTUPDATEUSER, LASTUPDATDATE, ISDELETED, DELETEDUSERNAME, DELETEDUSER, DELETEDDATE) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', NULL, NULL, NULL, NULL, '0', '管理员', '1', TO_DATE('2020-07-09 08:16:49', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL, '0', NULL, NULL, NULL);\n"
              , id,areaCode,areaName,areaName,level,parentCode,pid));
      if (cities!=null){
         for (City city: cities){
            city.print(id,appender);
         }
      }
   }
}
