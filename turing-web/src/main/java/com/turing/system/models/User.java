package com.turing.system.models;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author ygl
 * @since 2022-09-27 17:09:05
 */
@Getter
@Setter
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 删除标志
     */
    private Boolean hasDel;

    /**
     * 使用的邮箱
     */
    private String email;

    /**
     * 1男2女
     */
    private Integer gender;

    /**
     * 编号
     */
    private String number;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 注册类型（英文）
     */
    private String register;

    /**
     * 姓名
     */
    private String name;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 状态
     */
    private Integer status;

    private String orgCode;

    private String orgName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Long createUser;

    private Long updateUser;


}
