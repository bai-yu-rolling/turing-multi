package com.turing.system.service;

import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import com.turing.core.CanBeCapturedException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public interface ExcelService {


   <T> void template(HttpServletResponse response, Class<T> clazz, String name) throws IOException;

   <C extends IExcelModel & IExcelDataModel,R> int importByFile (MultipartFile file, Class<C> clazz, Function<List<C>, R> function, Function<C, Boolean> bef) throws Exception;

   <C extends IExcelModel & IExcelDataModel,R> int importByFileSax (MultipartFile file, Class<C> clazz, Function<List<C>, R> function, Function<C, Boolean> bef) throws CanBeCapturedException, InterruptedException, IOException;

   <T> void exportExcel(HttpServletResponse response, List<T> list, Class<T> clazz, String title) throws IOException;
}
