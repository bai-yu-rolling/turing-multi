package com.turing.system.service;
import com.turing.system.models.ImportRecordWrapper;
import com.turing.system.models.MainTableAware;

import javax.validation.Valid;
import java.io.InputStream;

public interface AsyncExcelService {


    /**
     * 具体的导入实现
     * @param file
     * @param wrapper
     * @param <T>
     */
    <T extends MainTableAware> void asyncImport(InputStream file, @Valid ImportRecordWrapper wrapper, ImportTransferStrategy<T> importTransferStrategy);

    /**
     * 将临时表数据转移到正式表
     * @param batchId
     * @return
     */
    int transfer(Long batchId, ImportTransferStrategy<? extends  MainTableAware> importTransfer);

    void nullify(Long batchId);



}
