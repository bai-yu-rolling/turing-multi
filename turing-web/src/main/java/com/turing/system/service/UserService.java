package com.turing.system.service;

import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.models.User;
import com.turing.system.params.UserParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ygl
 * @since 2022-09-27 17:09:05
 */
public interface UserService  {

    /**
    * 新增
    * @param user
    * @return 个数
    */
    int save(User user);

    /**
    * 更新
    * @param user
    * @return 个数
    */
    int update(User user);

    /**
    * 删除
    * @param id
    * @return 个数
    */
    int deleteById(Long id);

    /**
    * 根据id查询
    * @param id
    * @return User
    */
    User getById(Long id);


    /**
    * 列表查询
    * @param params
    * @return
    */
    PageInfo<User> list(PageParam<UserParam> params);


}
