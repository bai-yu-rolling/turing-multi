package com.turing.system.service;


import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.models.City;
import com.turing.system.models.CityPageParam;

import java.util.List;

/**
 * @author ygl
 * @date 2021/8/19 10:50
 */
public interface CityService {


    /**
     * 获取全部的城市
     * @return city
     */
    List<City> getAllForTree();

    /**
     * 获取全部的城市
     * @return city
     */
    List<City> getAll();

    /**
     * 刷新缓存
     */
    void refreshCache();


    /**
     * 根据parent_code 查询
     * @param pcd
     * @return city
     */
    List<City> getCityByPCD(String pcd);


    PageInfo<City> list(PageParam<CityPageParam> pageParam);

    void test();

}
