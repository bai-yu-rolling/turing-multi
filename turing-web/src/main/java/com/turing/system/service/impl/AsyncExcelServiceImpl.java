package com.turing.system.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.handler.inter.IReadHandler;
import com.truring.common.util.TuringThreadPoolUtils;
import com.turing.system.models.ImportRecord;
import com.turing.system.models.ImportRecordWrapper;
import com.turing.system.models.MainTableAware;
import com.turing.system.service.AsyncExcelService;
import com.turing.system.service.ImportRecordService;
import com.turing.system.service.ImportTransferStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Slf4j
@Service
@Validated
public class AsyncExcelServiceImpl implements AsyncExcelService {

    private static final int MAX_INSERT = 1000;

    private final ImportRecordService importRecordService;

    public AsyncExcelServiceImpl(ImportRecordService importRecordService) {
        this.importRecordService = importRecordService;
    }

    @Override
    @Async
    public <T extends MainTableAware> void asyncImport(InputStream file, @Valid ImportRecordWrapper wrapper, ImportTransferStrategy<T> importTransferStrategy) {

        long start = System.currentTimeMillis();

        ImportParams params = new ImportParams();
        params.setTitleRows(1);
        params.setSheetNum(Integer.MAX_VALUE);
        int cpu = Runtime.getRuntime().availableProcessors()/2;
        log.info("工作执行数：{}",cpu);

        ImportRecord record = new ImportRecord(wrapper);
        importRecordService.save(record);

        ThreadPoolExecutor pool = TuringThreadPoolUtils.init( cpu, cpu, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>(), "import-"+wrapper.getTitle()+"-");
        try {
            ExcelImportUtil.importExcelBySax(file, importTransferStrategy.getClazz(), params, new IReadHandler<T>() {
               ArrayList<T> bucket = new ArrayList<>(MAX_INSERT);
               int importCount = 0;
               @Override
               public void handler(T o) {
                   if (!importTransferStrategy.before(o)) return;
                   o.info(record);
                   bucket.add(o);
                   importCount++;
                   if (bucket.size() == MAX_INSERT){
                       ArrayList<T> temp = bucket;
                       bucket = new ArrayList<>(MAX_INSERT);
                       execute(importTransferStrategy::batch,temp,record.getId(),pool);
                   }
               }

               @Override
               public void doAfterAll() {
                   if (bucket.size() > 0 ){
                       execute(importTransferStrategy::batch,bucket,record.getId(),pool);
                   }
                   setStatus(ImportRecord.builder().setId(record.getId()).setImportCount(importCount));
                   stop(pool);
               }
           });
        }catch (Exception e){
            stop(pool);
            log.error("解析失败",e);
            setStatus(ImportRecord.builder().setId(record.getId()).setMessage(e.getMessage()).setStatus(ImportRecord.PARSED_ERROR));
            return;
        }
        try {
            importTransferStrategy.clean();
        }catch (Exception e){
            log.warn("清理临时表中的旧数据异常",e);
        }

        while (!pool.isTerminated()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.warn("意外的中断",e);
            }
        }
        long end = System.currentTimeMillis();
        log.info("执行结束:{}，线程池关闭：{}",end-start,pool.isTerminated());
        importRecordService.temporaryStorageEnd(record.getId());
    }

    private  <T extends MainTableAware>  void execute(Function<List<T>, Integer> worker, ArrayList<T> bucket, Long id, ThreadPoolExecutor pool) {
        pool.execute(()-> {
            try {
                Integer count = worker.apply(bucket);
                importRecordService.addCount(id, count);
            } catch (Exception e) {
                stop(pool);
                log.error("分批导入异常", e);
                setStatus(ImportRecord.builder().setId(id).setMessage(e.getMessage()).setStatus(ImportRecord.TEMPORARY_STORAGE_ERROR));
            }
        });
    }

    private void stop(ThreadPoolExecutor pool){
        if (!pool.isShutdown()){
            pool.shutdownNow();
        }
    }


    @Override
    public int transfer(Long batchId, ImportTransferStrategy<? extends  MainTableAware> importTransfer) {
        ImportRecord importRecord = importRecordService.getById(batchId);
        if (importRecord == null) throw new IllegalArgumentException("无此数据");
        if (!importRecord.getTempTableName().equals(importTransfer.getTempTableName()))throw new IllegalArgumentException("策略对象的临时表名与batchId数据记录的表名不一致");
        int status = importRecord.getStatus();
        if (importRecord.getType() != ImportRecord.FORMAL_TYPE || (status&ImportRecord.TEMPORARY_STORAGE) != status){
            throw new IllegalStateException("状态必须为临时存储，且数据类型为正常数据，请刷新后重试");
        }

        //通过数据库乐观锁，同步多人操作时的状态刷新问题，防止多次导入
        int count = setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.COMPARING).setVersion(importRecord.getVersion()));
        if (count <= 0) throw new IllegalStateException("当前状态以改变，请刷新后重试");
        try {
            int compareCount = importTransfer.compare(batchId);
            setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.COMPARED).setCompareResultCount(compareCount));
        }catch (Exception e){
            log.error("对比异常：",e);
            setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.COMPARED_ERROR).setMessage(e.getMessage()));
            throw new IllegalStateException("对比异常:"+e.getMessage());
        }
        int c = 0;
        Date curr = new Date();
        setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.IMPORTING));
        try {
            c = importTransfer.tempToFormal(batchId,curr);
        }catch (Exception e){
            log.error("导入失败:",e);
            setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.FORMAL_STORAGE_ERROR).setMessage(e.getMessage()));
            throw new RuntimeException("导入失败："+e.getMessage());
        }
        setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.FORMAL_STORAGE).setTransferTime(curr));
        return c;
    }

    @Override
    public void nullify(Long batchId) {
        ImportRecord importRecord = importRecordService.getById(batchId);
        if (importRecord == null) throw new IllegalArgumentException("无此数据");
        int status = importRecord.getStatus();
        if ( (status&ImportRecord.TEMPORARY_STORAGE) != status){
            throw new IllegalStateException("状态必须为临时存储");
        }
        int count = setStatus(ImportRecord.builder().setId(batchId).setStatus(ImportRecord.NULLIFY).setVersion(importRecord.getVersion()));
        if (count <= 0) throw new IllegalStateException("当前状态以改变，请刷新后重试");
    }

    private int setStatus(ImportRecord importRecord){
        return importRecordService.update(importRecord);
    }

}
