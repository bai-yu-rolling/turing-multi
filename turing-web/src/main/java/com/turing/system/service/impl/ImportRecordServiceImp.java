package com.turing.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.mapper.ImportRecordMapper;
import com.turing.system.models.ImportRecord;
import com.turing.system.params.ImportRecordParam;
import com.turing.system.service.ImportRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ygl
 * @since 2022-08-05 17:00:53
 */
@Service
@Slf4j
public class ImportRecordServiceImp implements ImportRecordService {

    private final ImportRecordMapper importRecordMapper;



    public ImportRecordServiceImp(ImportRecordMapper importRecordMapper) {
        this.importRecordMapper = importRecordMapper;
    }

    @Override
    public int save(ImportRecord importRecord) {
        return importRecordMapper.insert(importRecord);
    }

    @Override
    public int addCount(Long id ,int count) {
        return importRecordMapper.addCount(id,count);
    }

    @Override
    public int update(ImportRecord importRecord) {
        return importRecordMapper.update(importRecord);
    }

    @Override
    public int temporaryStorageEnd(Long id) {
        return importRecordMapper.temporaryStorageEnd(id);
    }

    @Override
    public int deleteById(Long id) {
        return importRecordMapper.deleteById(id);
    }

    @Override
    public ImportRecord getById(Long id) {
        return importRecordMapper.selectById(id);
    }

    @Override
    public PageInfo<ImportRecord> list(PageParam<ImportRecordParam> params) {
        return new PageInfo<>(importRecordMapper.selectByParams(params.getParam()));
    }




}
