package com.turing.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.models.User;
import com.turing.system.mapper.UserMapper;
import com.turing.system.params.UserParam;
import com.turing.system.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ygl
 * @since 2022-09-27 17:09:05
 */
@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int save(User user) {
        return userMapper.insert(user);
    }

    @Override
    public int update(User user) {
        return userMapper.update(user);
    }

    @Override
    public int deleteById(Long id) {
        return userMapper.deleteById(id);
    }

    @Override
    public User getById(Long id) {
        return userMapper.selectById(id);
    }

    @Override
    public PageInfo<User> list(PageParam<UserParam> params) {
        return new PageInfo<>(userMapper.selectByParams(params.getParam()));
    }



}
