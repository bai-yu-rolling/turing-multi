package com.turing.system.service;

import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.models.ImportRecord;
import com.turing.system.params.ImportRecordParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ygl
 * @since 2022-08-05 17:00:53
 */
public interface ImportRecordService {

    /**
    * 新增
    * @param importRecord
    * @return 个数
    */
    int save(ImportRecord importRecord);

    int addCount(Long id ,int count);

    /**
    * 更新
    * @param importRecord
    * @return 个数
    */
    int update(ImportRecord importRecord);

    int temporaryStorageEnd(Long id);

    /**
    * 删除
    * @param id
    * @return 个数
    */
    int deleteById(Long id);

    /**
    * 根据id查询
    * @param id
    * @return ImportRecord
    */
    ImportRecord getById(Long id);


    /**
    * 列表查询
    * @param params
    * @return
    */
    PageInfo<ImportRecord> list(PageParam<ImportRecordParam> params);


}
