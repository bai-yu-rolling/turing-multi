package com.turing.system.service;


import com.turing.system.models.MainTableAware;

import java.util.Date;
import java.util.List;

/**
 * 自定义的导入策略需要实现
 */
public interface ImportTransferStrategy<T extends MainTableAware> {

    boolean before(T obj);

    int batch(List<T> temps);


    /**
     *
     * 对原数据进行处理
     * @param batchId
     * @return 实际数量
     */
    int compare(Long batchId);

    /**
     * 从临时表到正式数据库
     * @param batchId
     * @param createTime
     * @return
     */
    int tempToFormal(Long batchId, Date createTime);


    /**
     * 清理数据，由导入模块决定何时调用，建议实现者，删除一段时间前的数据
     * @return
     */
    int clean();


    Class<T> getClazz();

    List<String> getTableName();

    String getTempTableName();

    String getRecordIdColumn();

    String name();

}
