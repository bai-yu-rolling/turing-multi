package com.turing.system.service.impl;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import cn.afterturn.easypoi.handler.inter.IReadHandler;
import com.truring.common.thread.BatchTransaction;
import com.truring.common.util.TuringThreadPoolUtils;
import com.turing.core.CanBeCapturedException;
import com.turing.system.service.ExcelService;
import lombok.Cleanup;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Service
public class ExcelServiceImp implements ExcelService {

    private static final ThreadPoolExecutor executorService = TuringThreadPoolUtils.init( 10, Integer.MAX_VALUE, 10, TimeUnit.MINUTES, new SynchronousQueue<>(), "batch-");;

    private static final int MAX_INSERT = 1000;
    private static final int LIMIT_THREAD = 100;
    private static final int TITLE_LINE = 1;
    private static final int HEAD_LINE = 1;


    private final PlatformTransactionManager platformTransactionManager;

    public ExcelServiceImp(PlatformTransactionManager platformTransactionManager) {
        this.platformTransactionManager = platformTransactionManager;
    }


    @Override
    public <T> void template(HttpServletResponse response, Class<T> clazz, String name) throws IOException {
        exportExcel(response,new ArrayList<>(),clazz,name);
    }

    @Override
    public <C extends IExcelModel & IExcelDataModel,R> int importByFile (MultipartFile file, Class<C> clazz, Function<List<C>,R> executor, Function<C,Boolean> bef) throws Exception {
        checkParam(file,clazz,executor);
        ImportParams importParams = getImportParams();
        ExcelImportResult<C> resultList = ExcelImportUtil.importExcelMore(file.getInputStream(),clazz, importParams);
        if (resultList.isVerifyFail()) {
            StringJoiner joiner = new StringJoiner("\r");
            List<C> failList = resultList.getFailList();
            if (failList != null && failList.size() > 0) {
                for (C excel : failList) {
                    joiner.add("第" + (excel.getRowNum() + 1) + "行：" + excel.getErrorMsg());
                }
            }
            throw  new CanBeCapturedException(joiner.toString());
        }
        List<C> result = resultList.getList();
        int size = result.size();
        if (size <= 0){
            return 0;
        }

        BatchTransaction<R> task =  new BatchTransaction<R>(executorService, platformTransactionManager);
        List<C> batch = new ArrayList<>(MAX_INSERT);
        int allCount = 0;
        int currArrCount = 0;
        for (C obj : result) {
            Boolean added = true;
            if (bef != null) {
                added = bef.apply(obj);
            }
            if (added) {
                batch.add(obj);
            }
            currArrCount = batch.size();
            if (currArrCount == MAX_INSERT ) {
                List<C> finalBatch = batch;
                task.add(() -> executor.apply(finalBatch));
                allCount += currArrCount;
                batch = new ArrayList<>(MAX_INSERT);
            }
        }
        if (batch.size() > 0){
            List<C> finalBatch = batch;
            task.add(()->executor.apply(finalBatch));
            allCount += batch.size();
        }
        if (allCount> LIMIT_THREAD * MAX_INSERT){
            throw  new CanBeCapturedException("数据超过10w条");
        }

        task.execute();
        if (task.getCommitOrNot()){
            return allCount;
        }else {
            throw  new CanBeCapturedException("服务器异常,提交被中断回滚");
        }

    }

    @Override
    public <C extends IExcelModel & IExcelDataModel, R> int importByFileSax(MultipartFile file, Class<C> clazz, Function<List<C>, R> executor, Function<C, Boolean> bef) throws CanBeCapturedException, InterruptedException, IOException {
        checkParam(file,clazz,executor);
        ImportParams importParams = getImportParams();
        BatchTransaction<R> task =  new BatchTransaction<R>(executorService, platformTransactionManager);
        final int[] allCount = {0};
        ExcelImportUtil.importExcelBySax(
                file.getInputStream(),
                clazz, importParams,
                new IReadHandler<C>() {
                    private List<C> batch = new ArrayList<>(MAX_INSERT);
                    private int currArrCount = 0;;
                    private int tempLine = TITLE_LINE + HEAD_LINE;
                    @Override
                    public void handler(C obj) {
                        Boolean added = true;
                        obj.setRowNum(++tempLine);
                        if (bef != null) {
                            added = bef.apply(obj);
                        }
                        if (added) {
                            batch.add(obj);
                        }
                        currArrCount = batch.size();
                        if (currArrCount == MAX_INSERT) {
                            List<C> finalBatch = batch;
                            task.add(() -> executor.apply(finalBatch));
                            allCount[0] += currArrCount;
                            batch = new ArrayList<>(MAX_INSERT);
                        }
                    }

                    @Override
                    public void doAfterAll() {
                        if (batch.size() > 0){
                            List<C> finalBatch = batch;
                            task.add(()->executor.apply(finalBatch));
                            allCount[0] += batch.size();
                        }
                    }
                });


        if (allCount[0]> LIMIT_THREAD * MAX_INSERT){
            throw  new CanBeCapturedException("数据超过10w条");
        }
        task.execute();
        if (task.getCommitOrNot()){
            return allCount[0];
        }else {
            throw  new CanBeCapturedException("服务器异常,提交被中断回滚");
        }

    }

    @Override
    public <T> void exportExcel(HttpServletResponse response, List<T> list, Class<T> clazz, String title) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename="+title+".xlsx");
        @Cleanup OutputStream ouputStream = response.getOutputStream();
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(title,title), clazz, list );
        workbook.write(ouputStream);
    }


    private void checkParam(Object ...o){
        for (Object obj: o) {
            if (obj == null)
                throw new IllegalArgumentException("参数错误");
        }
    }

    private ImportParams getImportParams(){
        ImportParams importParams = new ImportParams();
        //表格标题所占据的行数,默认0，代表没有标题
        importParams.setTitleRows(TITLE_LINE);
        //表头所占据的行数行数,默认1，代表标题占据一行
        importParams.setHeadRows(HEAD_LINE);
        // 需要验证(带字段验证的导入)
        importParams.setNeedVerify(true);
        return importParams;
    }
}
