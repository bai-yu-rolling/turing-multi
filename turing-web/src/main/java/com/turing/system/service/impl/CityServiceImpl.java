package com.turing.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.turing.core.PageParam;
import com.turing.system.mapper.CityMapper;
import com.turing.system.models.City;
import com.turing.system.models.CityPageParam;
import com.turing.system.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ygl
 * @date 2021/8/19 10:50
 */
@Slf4j
@Service
public class CityServiceImpl implements CityService {



    @Resource
    private CityMapper cityMapper;

    //当key不存在，默认key,city::SimpleKey []
    @Cacheable(cacheNames = {"city"},key = "'all'")
    @Override
    public List<City> getAllForTree() {
        Map<String,City> cache = new HashMap<>(2<<12);
        List<City> cities = cityMapper.selectAll();
        for (City c:cities){
            cache.put(c.getAreaCode(),c);
        }
        List<City> result = new LinkedList<>();
        for (City c:cities){
            City parent = cache.get(c.getParentCode());
            if (parent == null){
                result.add(c);
            }else {
                List<City> chil = parent.getCities();
                if (chil == null){
                    chil = new LinkedList<>();
                    parent.setCities(chil);
                }
                chil.add(c);
            }
        }

        return result;
    }

    @Override
    public List<City> getAll() {
        return cityMapper.selectAll();
    }


    @Override
    @CacheEvict(cacheNames = {"city","cityByCode"},allEntries=true)
    public void refreshCache() {
        log.info("\n=====删除缓存");
    }

    @Override
    @Cacheable(cacheNames = {"cityByCode"})
    public List<City> getCityByPCD(String pcd) {
        return cityMapper.selectByParentCode(pcd);
    }

    @Override
    public PageInfo<City> list(PageParam<CityPageParam> pageParam) {
        return new PageInfo<>(cityMapper.selectAll());
    }


    @Autowired
    private ApplicationContext applicationContext;

    //RC隔离级别是可以防止不可重复读的，毕竟用了MVCC(多版本并发控制)
    @Override
    @Transactional
//            (isolation = Isolation.READ_COMMITTED)
    public void test() {
//        List<Map<String,Object>> m = cityMapper.test();
//        log.info("{}",m);
        //此处不加条件的update会产生幻读，如果没有这个则不会有幻读
//        cityMapper.testUpdate("yang");
//        m = cityMapper.test();
//        log.info("{}",m);
        new Thread(this::t1).start();
        new Thread(this::t2).start();

        //事务扩展点
//        TransactionSynchronizationManager.registerSynchronization(new MyTransactionSynchronization());
//        applicationContext.publishEvent("123");
    }

    public void t1(){
        cityMapper.test();
    }
    public void t2(){
        cityMapper.test2();
    }


    static class MyTransactionSynchronization implements TransactionSynchronization {


        @Override
        public void afterCommit() {
            log.info("commit end");
        }
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMPLETION)
    public void prepareOrderCompletion(String orderId) {
        log.info("prepareOrderCompletion:" + orderId);
    }

}
