package com.turing.core.config;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author ygl
 * @description todo
 * @date 2021/10/30 11:21
 */
@Data
public class Register {
    @Excel(name = "ID",isImportField = "true")
    private String id;
    @Excel(name = "PAT_NAME",isImportField = "true")
    private String pat_name;
    @Excel(name = "ID_CARD",isImportField = "true")
    private String id_card;
    @Excel(name = "PHONE",isImportField = "true")
    private String phone;
    @Excel(name = "PUBLIC_NO_CODE",isImportField = "true")
    private String public_no_code;
    @Excel(name = "USER_ID",isImportField = "true")
    private String user_id;
    @Excel(name = "COUNTY_CODE",isImportField = "true")
    private String county_code;
    @Excel(name = "COUNTY_NAME",isImportField = "true")
    private String county_name;
    @Excel(name = "TOWN_CODE",isImportField = "true")
    private String town_code;
    @Excel(name = "TOWN_NAME",isImportField = "true")
    private String town_name;
    @Excel(name = "ADDRESS",isImportField = "true")
    private String address;
    @Excel(name = "PROFESSION",isImportField = "true")
    private String profession;
    @Excel(name = "PATIENT_ID",isImportField = "true")
    private String patient_id;
    @Excel(name = "FACE_RECOGNITION",isImportField = "true")
    private String face_recognition;
    @Excel(name = "CREATE_TIME",isImportField = "true")
    private String create_time;
    @Excel(name = "UPDATE_TIME",isImportField = "true")
    private String update_time;
    @Excel(name = "MEDICAL_CARD_NUMBER",isImportField = "true")
    private String medical_card_number;
    @Excel(name = "WORK_UNIT",isImportField = "true")
    private String work_unit;
    @Excel(name = "NATION",isImportField = "true")
    private String nation;

}
