package com.turing.core.config;

import com.truring.common.GenerateCode;


public class GenerateCodeTest {


    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5432/nucleic_acid?currentSchema=public&useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false";
        String username = "postgres";
        String pwd = "123456";
        GenerateCode.Builder.builderFactory(url,username,pwd)
                .setOutputDir("d:/project_out/3")
                .setParent("com.itun.cloud")
                .setModule("vehicle")
                .setTableName("sys_user")
                .setAuthor("ygl")
                .setEntity("model")
                .setMapper("dao")
//                        .setPrefix("jw")
                .setPrimaryType(String.class)
                .execute();
    }
}
