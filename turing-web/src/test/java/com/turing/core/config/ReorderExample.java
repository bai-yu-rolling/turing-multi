package com.turing.core.config;


/***
 * 以下都是共享数据，理论上是不可能产生：x==0&&y==0的情况，但要是单个cpu进行了指令重排，
 * x=b和y=a提前
 *
 *   x = 0;
 *   y = 0;
 *   a = 0;
 *   b = 0;
 *  // 执行线程1
 *   thread1() {
 *     a = 1;
 *     x = b;
 *   }
 *   // 执行线程2
 *   thread2() {
 *     b = 1;
 *     y = a;
 *   }
 */
public class ReorderExample {

    private static int num = 0;
    private static boolean ready = false;
     private static class ReaderThread extends Thread {
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            System.out.println(num);
        }
    }
     public static void main(String[] args) {
        new ReaderThread().start();
        num = 42;
        Thread.yield();
        ready = true;
    }
}