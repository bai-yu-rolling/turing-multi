package com.turing.core.config;

import lombok.Data;

@Data
public class RemoteWellModel {

             private String districtId;
             private String createTime;
             private String street;
             private String wellId;
             private String position;
             private String wellNo;
             private String wellMap;
             private String wellType;
}
