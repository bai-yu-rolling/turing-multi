package com.turing.core.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RemoteResponse<T> {


    private String test;

    private Integer code;

    private String msg;


    private T data;
}
