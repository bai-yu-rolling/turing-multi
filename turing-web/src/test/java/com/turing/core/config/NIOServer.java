package com.turing.core.config;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * @author ygl
 * @description
 * @date 2022/5/16 8:29
 */
public class NIOServer implements Runnable{

    private Selector selector;
    private ServerSocketChannel serverSocket;

    public NIOServer(int port) throws Exception {
        selector = Selector.open();
        serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(port));
        serverSocket.configureBlocking(false);
        SelectionKey sk = serverSocket.register(selector, SelectionKey.OP_ACCEPT);
//        sk.attach();
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                //阻塞等待事件
                selector.select();
                // 事件列表
                Set selected = selector.selectedKeys();
                Iterator it = selected.iterator();
                while (it.hasNext()) {
                    SelectionKey key = (SelectionKey) it.next();
                    it.remove();
                    //分发事件
                    dispatch(key);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dispatch(SelectionKey key) throws Exception {
        if (key.isAcceptable()) {
            System.out.println("新链接建立，注册");
            register(key);//新链接建立，注册
        } else if (key.isReadable()) {
            System.out.println("读事件处理");
            read(key);//读事件处理
        } else if (key.isWritable()) {
            System.out.println("写事件处理");
            write(key);//写事件处理
        }
    }

    private void read(SelectionKey key){
        try {
            SocketChannel channel = ((SocketChannel) key.channel());//拿到channel
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            int size = channel.read(buffer);//如果是正常断开，size = -1
            if(size == -1)
            {
                key.cancel();
                System.out.println("关闭");
            }
            else
            {
                buffer.flip();
                byte[] dst = new byte[buffer.limit()];
                buffer.get(dst);
                buffer.rewind();
                String r = new String(dst);
                if ("test".equals(r)){
                    test(channel);
                }else {
                    channel.write(buffer);
                }

                System.out.println(r);
            }
        }catch (Exception e){
            e.printStackTrace();
            key.cancel();
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel channel = ((SocketChannel) key.channel());//拿到channel
        ByteBuffer buffer = (ByteBuffer) key.attachment();
        int c = channel.write(buffer);
        System.out.println("发送:"+c);
        if (!buffer.hasRemaining()){
            key.attach(null);
            key.interestOps(SelectionKey.OP_READ);
        }

    }

    private void test( SocketChannel channel) throws IOException {

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 9999999; i++) {
            sb.append("a");
        }

        ByteBuffer bf = Charset.defaultCharset().encode(sb.toString());
        if (bf.hasRemaining()){
//            int c = channel.write(bf);
//            System.out.println("发送:"+c);
            channel.register(selector,SelectionKey.OP_WRITE,bf);
        }

    }

    private void register(SelectionKey key) throws Exception {
        ServerSocketChannel server = (ServerSocketChannel) key
                .channel();
        // 获得和客户端连接的通道
        SocketChannel channel = server.accept();
        channel.configureBlocking(false);
        //客户端通道注册到selector 上
        channel.register(this.selector, SelectionKey.OP_READ);
    }


    public static void main(String[] args) throws Exception {

        System.out.println("start");
        new NIOServer(8081).run();
        System.out.println("end");

    }


}
