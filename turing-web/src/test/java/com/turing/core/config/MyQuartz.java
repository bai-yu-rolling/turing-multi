package com.turing.core.config;


import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class MyQuartz extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getMergedJobDataMap();

        //具体要干的事情
        System.out.println("具体要干的事情...");
        System.out.println(data.get("abac"));
        System.out.println(data.get("trigge"));
    }
}