package com.turing.core.config;

import com.turing.test.Registry;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.sql.Driver;

public class ServiceLoaderTest {
 
    public static void main(String[] args) {
 
        ServiceLoader<Registry> load = ServiceLoader.load(Registry.class);
        for (Registry registry : load) {
            registry.registry("127.0.0.1", 10086);
        }
    }
}