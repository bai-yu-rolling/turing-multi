package com.turing.core.config;

import com.turing.system.quartz.CronPointScheduleBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QuartzTest {


    @Autowired
    private Scheduler scheduler;

    @Test
    public void test1() throws SchedulerException, InterruptedException {
        //获取调度工厂
//        SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
        //从工厂中获取调度器
//        Scheduler sched = schedFact.getScheduler();
//        scheduler.start();
        JobDetail jobDetail = createJob();

        LocalTime now = LocalTime.now();
        CronPointScheduleBuilder schedBuilder3 = CronPointScheduleBuilder.cronPointSchedule("0 0 0 28 8 ? 2023", new LocalTime[]{now.plus(10, ChronoUnit.SECONDS)})
                .withMisfireHandlingInstructionDoNothing();

        Trigger trigger3 = TriggerBuilder.newTrigger().forJob(jobDetail).withSchedule(schedBuilder3)
                .withIdentity("考勤组1_班次2","sys/bc/qid")
                .usingJobData("trigge","我是2")
                .build();
//        scheduler.scheduleJob(jobDetail,trigger);
        scheduler.scheduleJob(trigger3);

        Thread.sleep(60000);
    }

    @Test
    public void test3() throws InterruptedException, SchedulerException {
        JobDetail jobDetail = createJob();

        ScheduleBuilder schedBuilder = CronScheduleBuilder.cronSchedule("0 0 0 1 1 ? 2024");

        Trigger trigger = TriggerBuilder.newTrigger().forJob(jobDetail).withSchedule(schedBuilder)
                .withIdentity("考勤组1_班次1","sys/bc/qid")
                .usingJobData("trigge","我是1")
                .build();

        scheduler.scheduleJob(trigger);


        ScheduleBuilder schedBuilder2 = CronScheduleBuilder.cronSchedule("0/10 * * * * ?");
        Trigger trigger2 = TriggerBuilder.newTrigger().forJob(jobDetail).withSchedule(schedBuilder2)
                .withIdentity("考勤组1_班次2","sys/bc/qid")
                .usingJobData("trigge","我是2")
                .build();

        scheduler.scheduleJob(trigger2);
        Thread.sleep(60000);
    }

    private JobDetail createJob() throws SchedulerException {
        Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.anyJobGroup());
        scheduler.deleteJobs(new ArrayList<>(jobKeys));
        JobDetail jobDetail = JobBuilder.newJob(MyQuartz.class).storeDurably().withIdentity("考勤组1打卡提醒","sys/kq/qid")
                .usingJobData("abac","agagagag")
                .build();

        scheduler.addJob(jobDetail,false);
        return jobDetail;
    }

    @Test
    public void test10() throws InterruptedException {
        Thread.sleep(60000);
    }




}
