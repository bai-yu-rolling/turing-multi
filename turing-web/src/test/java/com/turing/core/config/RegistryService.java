//package com.turing.core.config;
//
//
//import com.sun.jndi.rmi.registry.ReferenceWrapper;
//
//import javax.naming.NamingException;
//import javax.naming.Reference;
//import java.rmi.AlreadyBoundException;
//import java.rmi.RemoteException;
//import java.rmi.registry.LocateRegistry;
//import java.rmi.registry.Registry;
//
//public class RegistryService {
//    public static void main(String[] args) {
//        try {
//            // 本地主机上的远程对象注册表Registry的实例,默认端口1099
//            Registry registry = LocateRegistry.createRegistry(1099);
//            System.out.println("======= 启动RMI服务! =======");
//            Reference reference = new Reference("com.turing.core.config.Hello","com.turing.core.config.Hello",null);
//            ReferenceWrapper referenceWrapper = new ReferenceWrapper(reference);
//            registry.bind("Hello",referenceWrapper);
//        } catch (RemoteException | NamingException | AlreadyBoundException e) {
//            e.printStackTrace();
//        }
//    }
//}