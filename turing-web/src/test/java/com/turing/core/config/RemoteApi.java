package com.turing.core.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class RemoteApi {


    private static final RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory());


    /**
     * get方法
     * @param url 地址
     * @param reference 返回值类型
     * @param <T> 返回值类型
     * @return 返回自定义的对象
     * @throws HttpServerErrorException 只针对非200的抛出异常
     */
    public static <T> T get(String url, ParameterizedTypeReference<T> reference) throws HttpServerErrorException {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.set("User-Agent", "PostmanRuntime/7.26.8");
        HttpEntity<Map<String,String>> request = new HttpEntity<>(headers);
        ResponseEntity<T> result = restTemplate.exchange(url,HttpMethod.GET,request,reference);
        check(result);
        return result.getBody();
    }

    /**
     * post方法，ContentType为"application/json"，采用jackson进行解析 {@link ObjectMapper#writeValueAsString}
     * @param url 地址
     * @param reference 返回值类型
     * @param jsonBody 参数对象，会自动解析为json
     * @param <T> 返回值类型
     * @return 返回自定义的对象
     * @throws JsonProcessingException json解析异常
     * @throws HttpServerErrorException 只针对非200的抛出异常
     */
    public static <T> T post(String url, ParameterizedTypeReference<T> reference, Object jsonBody) throws JsonProcessingException,  HttpServerErrorException{
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.set("User-Agent", "PostmanRuntime/7.26.8");

        ObjectMapper mapper=new ObjectMapper();
        HttpEntity<String> request = new HttpEntity<>(mapper.writeValueAsString(jsonBody), headers);

        ResponseEntity<T> result = restTemplate.exchange(url,HttpMethod.POST,request,reference);
        check(result);
        return result.getBody();
    }

    private static void check(ResponseEntity<?> result) throws HttpServerErrorException{
        HttpStatus statusCode = result.getStatusCode();
        if (!statusCode.is2xxSuccessful()){
            log.error("请求失败：{}",result);
            throw new HttpServerErrorException(statusCode,statusCode.getReasonPhrase());
        }
    }

    public static void main(String[] args) {
        RemoteResponse<List<RemoteWellModel>> response = get("http://222.174.52.222:9672/api/jobwell/getWellMsgList?objCode=MIICdwIBADANBgk",new ParameterizedTypeReference<RemoteResponse<List<RemoteWellModel>>>() {});


        System.out.println(response);
    }

}
