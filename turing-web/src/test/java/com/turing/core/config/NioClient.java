package com.turing.core.config;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class NioClient {

    public static void main(String[] args) {
        try{
            SocketChannel socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress(8081));//服务端就是bind  然后accept  serverSocketChannel

            Selector selector = Selector.open();

            socketChannel.register(selector, SelectionKey.OP_CONNECT);//注册连接事件

            while(true) {
                int number = selector.select();

                if(number > 0) {
                    Set<SelectionKey> selectionKeySet =  selector.selectedKeys();

                    Iterator<SelectionKey> iterable = selectionKeySet.iterator();
                    while(iterable.hasNext()) {//有事件发生
                        SelectionKey selectionKey = iterable.next();

                        SocketChannel client = (SocketChannel) selectionKey.channel();
                        if(selectionKey.isConnectable()) {//判断 selectionkey 状态  可连接的
                            if(client.isConnectionPending()) {//是否在准备连接的进程中
                                client.finishConnect();//这里会阻塞，如果连接未建立，抛异常 ，

                                ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

                                byteBuffer.put((LocalDateTime.now() + "，连接成功").getBytes());
                                byteBuffer.flip();
                                client.write(byteBuffer);

                                ExecutorService executorService = Executors.newSingleThreadExecutor(Executors.defaultThreadFactory());

                                executorService.submit(() -> {//起一个新的线程，去接收控制台的输入 ，不影响其他线程
                                    while(true) {
                                        try{
                                            byteBuffer.clear();
                                            InputStreamReader inputStreamReader = new InputStreamReader(System.in);
                                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                                            String in = bufferedReader.readLine();
                                            if ("exit".equals(in)){
                                                client.close();
                                                break;
                                            }
//                                            System.out.println(in);
                                            byteBuffer.put(in.getBytes());
//                                            byteBuffer.put(LocalDateTime.now().toString().getBytes());
                                            byteBuffer.flip();
                                            int c =
                                                    client.write(byteBuffer);
                                            System.out.println(c);
//                                            Thread.sleep(1000);

                                        }catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }

                            iterable.remove();
                            client.register(selector, SelectionKey.OP_READ);//注册读事件
                        } else if(selectionKey.isReadable()){//可读取
                            ByteBuffer readBuffer = ByteBuffer.allocate(1024);

                            int readCount = client.read(readBuffer);
                            System.out.println("size:"+readCount);
                            if(readCount > 0) {
                                readBuffer.flip();
                                byte[] dst = new byte[readBuffer.limit()];
                                readBuffer.get(dst);
                                String receiveMsg = new String(dst);
//                                System.out.println("receiveMsg : " + receiveMsg);
                            }

                            iterable.remove();
                        }

                    }
                }
            }




        }catch (Exception e ) {
            e.printStackTrace();
        }


    }

}