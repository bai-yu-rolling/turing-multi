package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import ${package.Other}.${entity}Param;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.turing.core.Result;
import com.github.pagehelper.PageInfo;
import com.turing.core.PageResult;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>


    @Autowired
    private  ${table.serviceName}  ${table.serviceName?uncap_first};

    @PostMapping("/${entity?uncap_first}")
    public Result<Integer> add(@RequestBody ${entity} ${entity?uncap_first}){
        return Result.success(${table.serviceName?uncap_first}.save(${entity?uncap_first}));
    }

    @PutMapping("/${entity?uncap_first}")
    public Result<Integer> update(@RequestBody ${entity} ${entity?uncap_first}){
        return Result.success(${table.serviceName?uncap_first}.update(${entity?uncap_first}));
    }

    @DeleteMapping("/${entity?uncap_first}/{id}")
    public Result<Integer> delete(@PathVariable ${primaryType} id){
        return Result.success(${table.serviceName?uncap_first}.deleteById(id));
    }

    @GetMapping("/${entity?uncap_first}/{id}")
    public Result<${entity}> getById(@PathVariable ${primaryType} id){
        return Result.success(${table.serviceName?uncap_first}.getById(id));
    }

     @GetMapping("/${entity?uncap_first}/list")
     public PageResult list(${entity}Param param){
        PageInfo pageInfo = ${table.serviceName?uncap_first}.list(param);
        return PageResult.success(pageInfo);
     }


}
</#if>
