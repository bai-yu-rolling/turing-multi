package ${package.Mapper};

import ${package.Entity}.${entity};
<#if mapperAnnotation>
import org.apache.ibatis.annotations.Mapper;
</#if>
import java.util.List;
import org.apache.ibatis.annotations.Param;
import ${package.Other}.${entity}Param;

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if mapperAnnotation>
@Mapper
</#if>
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName}{


    /**
     * 新增
     * @param ${entity?uncap_first}
     * @return 个数
     */
    int insert(${entity} ${entity?uncap_first});

    /**
     * 批量新增
     * @param ${entity?uncap_first}List
     * @return 个数
     */
    int insertBatch(@Param("list") List<${entity}> ${entity?uncap_first}List);

    /**
     * 删除
     * @param id
     * @return 个数
     */
    int deleteById(@Param("id") ${primaryType} id);

    /**
     * 批量删除
     * @param ids
     * @return 个数
     */
    int deleteBatchId(@Param("ids") List<${primaryType}> ids);

    /**
     * 更新
     * @param ${entity?uncap_first}
     * @return 个数
     */
    int update(${entity} ${entity?uncap_first});

    /**
     * 根据id查询
     * @param id
     * @return ${entity}
     */
    ${entity} selectById(@Param("id") ${primaryType} id);


    /**
    * 参数查询
    * @param params
    * @return
    */
    List<${entity}> selectByParams(${entity}Param params);

}
</#if>
