package ${package.Other};

import com.turing.core.PageParam;

import lombok.Getter;
import lombok.Setter;
/**
* <p>
* ${table.comment!} 查询参数
* </p>
*
* @author ${author}
* @since ${date}
*/
@Getter
@Setter
public class ${entity}Param extends PageParam<${entity}Param> {

}
