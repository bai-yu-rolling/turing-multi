package ${package.Service};

import ${package.Entity}.${entity};
import com.github.pagehelper.PageInfo;
import ${package.Entity}.${entity}Param;
import com.turing.core.PageParam;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName}  {

    /**
    * 新增
    * @param ${entity?uncap_first}
    * @return 个数
    */
    int save(${entity} ${entity?uncap_first});

    /**
    * 更新
    * @param ${entity?uncap_first}
    * @return 个数
    */
    int update(${entity} ${entity?uncap_first});

    /**
    * 删除
    * @param id
    * @return 个数
    */
    int deleteById(${primaryType} id);

    /**
    * 根据id查询
    * @param id
    * @return ${entity}
    */
    ${entity} getById(${primaryType} id);


    /**
    * 列表查询
    * @param params
    * @return
    */
    PageInfo<${entity}> list(PageParam<${entity}Param> params);


}
</#if>
