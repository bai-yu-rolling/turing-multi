<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">

<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="${cacheClassName}"/>

</#if>
<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
<#list table.fields as field>
<#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
<#list table.commonFields as field><#--生成公共字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#list>
<#list table.fields as field>
<#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
    </resultMap>

</#if>
<#if baseColumnList>
    <!-- 通用查询结果列 -->
    <sql id="${table.name}_field">
        <#list table.commonFields as field>
                ${field.columnName},
        </#list>
        ${table.fieldNames}
    </sql>

    <!--  新增  -->
    <insert id="insert" parameterType="${package.Entity}.${entity}">
        insert into ${table.name}
        <trim prefix="(" suffix=")" suffixOverrides=",">
                <#list table.fields as field>
                    <#if field.keyFlag>
            <if test="${field.propertyName}!=null">
                ${field.name},
            </if>
                    </#if>
                </#list>
                <#list table.commonFields as field>
            <if test="${field.propertyName}!=null">
                ${field.name},
            </if>
                </#list>
                <#list table.fields as field>
                    <#if !field.keyFlag>
            <if test="${field.propertyName}!=null">
                ${field.name},
            </if>
                    </#if>
                </#list>
        </trim>
        values
        <trim prefix="(" suffix=")" suffixOverrides=",">
                <#list table.fields as field>
                    <#if field.keyFlag>
            <if test="${field.propertyName}!=null">
                <#noparse>#</#noparse>{${field.propertyName}},
            </if>
                    </#if>
                </#list>
                <#list table.commonFields as field>
            <if test="${field.propertyName}!=null">
                <#noparse>#</#noparse>{${field.propertyName}},
            </if>
                </#list>
                <#list table.fields as field>
                    <#if !field.keyFlag>
            <if test="${field.propertyName}!=null">
                <#noparse>#</#noparse>{${field.propertyName}},
            </if>
                    </#if>
                </#list>
        </trim>
    </insert>

    <!--  批量新增  -->
    <insert id="insertBatch"  parameterType="${package.Entity}.${entity}">
        insert into ${table.name}
            (
            <#list table.fields as field>
                <#if field.keyFlag>
                ${field.name},
                </#if>
            </#list>
            <#list table.commonFields as field>
                ${field.name},
            </#list>
            <#list table.fields as field>
                <#if !field.keyFlag>
                ${field.name}<#if field_has_next>,</#if>
                </#if>
            </#list>
            )
        values
        <foreach  collection="list" item="item"  separator=",">
            (
               <#list table.fields as field>
                    <#if field.keyFlag>
                    <#noparse>#</#noparse>{item.${field.propertyName}},
                    </#if>
                </#list>
                <#list table.commonFields as field>
                    <#noparse>#</#noparse>{item.${field.propertyName}},
                </#list>
                <#list table.fields as field>
                    <#if !field.keyFlag>
                    <#noparse>#</#noparse>{item.${field.propertyName}}<#if field_has_next>,</#if>
                    </#if>
                </#list>
            )
        </foreach>
    </insert>

    <!--  更新  -->
    <update id="update" parameterType="${package.Entity}.${entity}">
        update ${table.name}
        <set>
            <#list table.fields as field>
                <#if field.keyFlag>
            <if test="${field.propertyName}!=null">
                ${field.name} =  <#noparse>#</#noparse>{${field.propertyName}},
            </if>
                </#if>
            </#list>
            <#list table.commonFields as field>
            <if test="${field.propertyName}!=null">
                ${field.name} =  <#noparse>#</#noparse>{${field.propertyName}},
            </if>
            </#list>
            <#list table.fields as field>
                <#if !field.keyFlag>
            <if test="${field.propertyName}!=null">
                ${field.name} =   <#noparse>#</#noparse>{${field.propertyName}},
            </if>
                </#if>
            </#list>
        </set>
        where id =  <#noparse>#</#noparse>{id}
    </update>

    <!--  删除  -->
    <delete id="deleteById">
        delete from ${table.name}
        where
        id =  <#noparse>#</#noparse>{id}
    </delete>

    <!--  批量删除  -->
    <delete id="deleteBatchId">
        delete from ${table.name}
        where
        id in (
        <foreach collection="ids" item="id" separator=",">
            <#noparse>#</#noparse>{id}
        </foreach>
        )
    </delete>

    <!--  根据id查询  -->
    <select id="selectById" resultType="${package.Entity}.${entity}">
        select
        <include refid="${table.name}_field"></include>
        from ${table.name}
        where id = <#noparse>#</#noparse>{id}
    </select>

    <!--  根据参数查询  -->
    <select id="selectByParams" resultType="${package.Entity}.${entity}">
        select
        <include refid="${table.name}_field"></include>
        from ${table.name}
    </select>

</#if>
</mapper>
