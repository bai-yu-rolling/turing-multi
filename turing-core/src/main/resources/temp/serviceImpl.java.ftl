package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.PageInfo;
import ${package.Entity}.${entity}Param;
import com.turing.core.PageParam;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} implements ${table.serviceName} {

    @Autowired
    private ${table.mapperName} ${table.mapperName?uncap_first};

    @Override
    public int save(${entity} ${entity?uncap_first}) {
        return ${table.mapperName?uncap_first}.insert(${entity?uncap_first});
    }

    @Override
    public int update(${entity} ${entity?uncap_first}) {
        return ${table.mapperName?uncap_first}.update(${entity?uncap_first});
    }

    @Override
    public int deleteById(${primaryType} id) {
        return ${table.mapperName?uncap_first}.deleteById(id);
    }

    @Override
    public ${entity} getById(${primaryType} id) {
        return ${table.mapperName?uncap_first}.selectById(id);
    }

    @Override
    public PageInfo<${entity}> list(PageParam<${entity}Param> params) {
        return new PageInfo<>(${table.mapperName?uncap_first}.selectByParams(params.getParam()));
    }



}
</#if>
