package com.turing.core.filter;

import java.util.Map;

public interface ExceptionAdviceCustomizers {

    Map<Class<? extends Exception>,Integer>  exceptionAndStatusMapping();



}
