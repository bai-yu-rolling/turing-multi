package com.turing.core.filter;

import com.turing.core.CanBeCapturedException;
import com.turing.core.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author ygl
 * 统一的异常返回处理
 */
@RestControllerAdvice
@Slf4j
public class SystemResponseExceptionAdvice {


    private static final Map<Class<? extends Exception>,Integer> EX_STATUS = new HashMap<>();

    static {
        //自定义的CanBeCapturedException返回200
        EX_STATUS.put(CanBeCapturedException.class, HttpServletResponse.SC_OK);

        /**
         * 以下照抄spring
         * {@link DefaultHandlerExceptionResolver#doResolveException}
         */
        EX_STATUS.put(HttpRequestMethodNotSupportedException.class,HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        EX_STATUS.put(HttpMediaTypeNotSupportedException.class,HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
        EX_STATUS.put(HttpMediaTypeNotAcceptableException.class,HttpServletResponse.SC_NOT_ACCEPTABLE);
        EX_STATUS.put(MissingPathVariableException.class,HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        EX_STATUS.put(MissingServletRequestParameterException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(ServletRequestBindingException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(ConversionNotSupportedException.class,HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        EX_STATUS.put(TypeMismatchException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(HttpMessageNotReadableException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(HttpMessageNotWritableException.class,HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        EX_STATUS.put(MethodArgumentNotValidException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(MissingServletRequestPartException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(BindException.class,HttpServletResponse.SC_BAD_REQUEST);
        EX_STATUS.put(NoHandlerFoundException.class,HttpServletResponse.SC_NOT_FOUND);
        EX_STATUS.put(AsyncRequestTimeoutException.class,HttpServletResponse.SC_SERVICE_UNAVAILABLE);
    }


    public SystemResponseExceptionAdvice(List<ExceptionAdviceCustomizers> exceptionAdviceCustomizers) {
        if (exceptionAdviceCustomizers == null) return;
        for (ExceptionAdviceCustomizers e : exceptionAdviceCustomizers){
            EX_STATUS.putAll(e.exceptionAndStatusMapping());
        }
    }


    /**
     * 同一拦截
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Result<?>> handleException( Exception ex) {
        Integer status = EX_STATUS.get(ex.getClass());
        if (status == null || HttpServletResponse.SC_OK != status){
            log.error("统一异常返回：", ex);
        }
        if (status == null) status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        return ResponseEntity.status(status).body(Result.failed(ex.getMessage()));
    }


}
