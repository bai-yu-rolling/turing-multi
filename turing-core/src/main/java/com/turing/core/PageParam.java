package com.turing.core;


import lombok.Data;

/**
 * 分页参数
 * @param <T>
 */
@Data
public abstract class PageParam<T extends PageParam<T>> {

    protected int pageNum = 1;

    protected int pageSize = 10;

    public T getParam(){
        return (T) this;
    }


}
