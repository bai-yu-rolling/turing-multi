package com.turing.core.redis;

import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author ygl
 * @date 2021/9/23 16:11
 */
public interface ExtentRedisSerializer extends RedisSerializer<Object> {

    /**
     * 区别与GenericJackson2JsonRedisSerializer,当前这种方式反序列化会变成Map
     * @return Jackson2JsonRedisSerializer
     */
    @Deprecated
    static RedisSerializer<Object> json2(){
        return new Jackson2JsonRedisSerializer<>(Object.class);
    }
}
