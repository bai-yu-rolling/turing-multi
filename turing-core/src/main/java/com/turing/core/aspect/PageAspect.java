package com.turing.core.aspect;

import com.github.pagehelper.PageHelper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 分页AOP
 * @author ygl
 */
@Aspect
@Component
public class PageAspect {

    @Pointcut("execution(public com.github.pagehelper.PageInfo com.turing.*.service..*(com.turing.core.PageParam))")
    public void pointcut(){

    }

    @Before("pointcut()")
    public void beforePage(JoinPoint joinPoint){
        Object[] params = joinPoint.getArgs();
        PageHelper.startPage(params[0]);
    }

    @AfterThrowing(pointcut = "pointcut()")
    public void afterThrowing(){
        PageHelper.clearPage();
    }

    @After("pointcut()")
    public void afterPage(){
        PageHelper.clearPage();
    }

}
