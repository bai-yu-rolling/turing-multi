package com.turing.core;


/**
 * @author ygl
 * @description 返回封装
 * @date 2021/9/27 8:30
 */
public class Result<T> {


    ResultStatus status;

    private String message;

    T data;

    public Result(){

    }

    public Result(ResultStatus status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }




    public static <R> Result<R> success(R data){
        return new Result<>(ResultStatus.success,"成功",data);
    }


    public static <R> Result<R> success(R data, String message){
        return new Result<>(ResultStatus.success,message,data);
    }

    public static <R> Result<R> failed(String message){
        return new Result<>(ResultStatus.failed,message,null);
    }


    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
