package com.turing.core.config;

import com.turing.core.redis.JsonRedisTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;


/**
 * Jedis是Redis的Java实现的客户端，其API提供了比较全面的Redis命令的支持。支持基本的数据类型如：String、Hash、List、Set、Sorted Set。
 * Lettuce高级Redis客户端，用于线程安全同步，异步和响应使用，支持集群，Sentinel，管道和编码器。
 * Redisson实现了分布式和可扩展的Java数据结构。Redisson不仅提供了一系列的分布式Java常用对象，基本可以与Java的基本数据结构通用，还提供了许多分布式服务。
 * @author ygl
 * @date 2021/9/23 9:52
 */
@Slf4j
@Configuration
@ConditionalOnClass({RedisConnectionFactory.class})
class RedisConfig {

    @Bean
    @ConditionalOnMissingBean
    public JsonRedisTemplate jsonRedisTemplate(RedisConnectionFactory factory) {
        JsonRedisTemplate mt = new JsonRedisTemplate();
        mt.setConnectionFactory(factory);
        log.debug("****************************************************************启用JsonRedisTemplate：{}*******************************************************************",mt);
        return mt;
    }


    @Autowired
    public void init(
            StringRedisTemplate stringRedisTemplate
            ,RedisTemplate  redisTemplate
    ){
        log.debug("****************************************************************启用RedisTemplate：{}*******************************************************************",redisTemplate);
        log.debug("****************************************************************启用StringRedisTemplate：{}*******************************************************************",stringRedisTemplate);
    }


}
