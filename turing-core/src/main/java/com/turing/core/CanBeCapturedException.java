package com.turing.core;

/**
 * @author ygl
 * @description 自定义捕获异常
 * @date 2021/8/31 15:54
 */
public class CanBeCapturedException extends RuntimeException  {

    public CanBeCapturedException() {
        super();
    }

    public CanBeCapturedException(String message) {
        super(message);
    }

    public CanBeCapturedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CanBeCapturedException(Throwable cause) {
        super(cause);
    }

    protected CanBeCapturedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
