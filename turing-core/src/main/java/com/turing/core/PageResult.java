package com.turing.core;

import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author ygl
 * @description 分页返回
 * @date 2021/9/28 14:26
 */
public class PageResult<T> extends Result<List<T>>{


    private long count;

    private boolean hasNextPage;


    public PageResult(List<T> data, boolean hasNextPage) {
        this.count = data.size();
        this.hasNextPage = hasNextPage;
        this.data = data;
        this.status = ResultStatus.success;
    }

    public PageResult(PageInfo<T> pageInfo){
        this.setCount(pageInfo.getTotal());
        this.setHasNextPage(pageInfo.isHasNextPage());
        this.setData(pageInfo.getList());
        this.status = ResultStatus.success;
    }

    public static <R> PageResult<R> success(PageInfo<R> pageInfo){
        return new PageResult<>(pageInfo);
    }


    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }
}
