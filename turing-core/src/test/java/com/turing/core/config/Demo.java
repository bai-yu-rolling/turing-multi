package com.turing.core.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.data.util.Lazy;

public class Demo {

	public static class A{
		private String a;

		public String getA() {
			return a;
		}

		public void setA(String a) {
			this.a = a;
		}
	}

	public static void main(String[] args) throws JsonProcessingException {
//		Lazy
	}
	
	public static void print(String content){
		System.out.println(content);
	}
}