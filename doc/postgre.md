### 1、创建原始表（用于对照）
```sql
    CREATE TABLE qh_na_record ( na_time timestamp);
```
### 2、将原始表创建为分区表
```sql
    CREATE TABLE qh_na_record ( na_time timestampt) PARTITION BY RANGE (na_time);
```
### 3、创建分区
```sql
    create table fq_2 PARTITION of qh_na_record for values FROM ('2022-08-01') to ('2022-9-1')
```
### 4、摘出分区
```sql
    alter table qh_na_record detach PARTITION fq_2
```
### 5、挂载分区
```sql
    alter table qh_na_record attach PARTITION fq_2 for values FROM ('2022-08-01') to ('2022-9-1')
```
### 6、创建索引
```sql
    create INDEX fq_2_na_time_index on fq_2 USING btree(na_time)
    CREATE UNIQUE INDEX name ON table (column [, ...]);
    DROP INDEX "public"."tt";
```
### 7、查询索引
```sql
    select 
        relname, indexrelname, idx_scan, idx_tup_read, idx_tup_fetch 
    from
        pg_stat_user_indexes
    where
        relname = 'fq_2'
    order by
        idx_scan asc, idx_tup_read asc, idx_tup_fetch asc;
```
