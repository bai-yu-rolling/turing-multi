## Linux服务器时间如果不正常，可以进行时间设置
```shell
yum -y install ntp # 若无ntpdate命令，安装ntp

ntpdate 1.cn.pool.ntp.org #  校正服务器时间同网络时间一致

tzselect #  设置时区，根据选择命令分别选择，5Asia->9 China->1->Beijing Time ->1 yes

TZ='Asia/Shanghai'; export TZ #  拷贝入系统配置文件中

hwclock -w #  软件时间同步至硬件时间

date #  查看当前时间
```

