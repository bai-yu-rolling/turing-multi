/**
 *
 *  postgreSql查询主键，多个主键时只查询第一个
 *
 *
 */
SELECT
A.attname AS name,
format_type (A.atttypid,A.atttypmod) AS type,
col_description (A.attrelid,A.attnum) AS comment,
(CASE WHEN (SELECT COUNT (*)
FROM pg_constraint AS PC
WHERE
A.attnum = PC.conkey[1]
and C.oid = PC.conrelid
AND PC.contype = 'p') > 0 THEN 'PRI' ELSE '' END) AS key
FROM pg_class AS C,
pg_attribute AS A
WHERE A.attrelid='"sys_user"'::regclass
AND A.attrelid= C.oid
AND A.attnum> 0
AND NOT A.attisdropped
ORDER BY A.attnum