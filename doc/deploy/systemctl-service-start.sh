#!/bin/bash

nohup java -jar -Xmx12288M -Xms12288M -XX:MaxMetaspaceSize=2048M -XX:MetaspaceSize=2048M -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+ParallelRefProcEnabled -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=dump.hprof -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:gcMonomer.log $1 --server.port=$2 --spring.profiles.active=$3 > $4 &
