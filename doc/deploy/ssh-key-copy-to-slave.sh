#!/bin/bash
while getopts f: OPT; do
  case $OPT in
  f | +f)
    files="$OPTARG $files"
    ;;
  *)
    echo "usage: $(basename $0) [-f hostfile] <from> <to>"
    exit 2
    ;;
  esac
done
shift $(expr $OPTIND - 1)

if [ "" = "$files" ]; then
  echo "usage: $(basename $0) [-f hostfile] <from> <to>"
  exit
fi

# 判断id_rsa密钥文件是否存在
if [ ! -f ~/.ssh/id_rsa ];then
 ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa
else
 echo "id_rsa has created ..."
fi


for file in $files; do
  if [ ! -f "$file" ]; then
    echo "no hostlist file:$file"
    exit
  fi
  hosts="$hosts $(cat $file)"
done


for host in $hosts; do
  echo "do $host"
  expect <<EOF
      set timeout 10
      spawn ssh-copy-id -i /root/.ssh/id_rsa.pub root@$host -p 9922
      expect {
	  "*yes/no" { send "yes\r"}
	  "*password:" {send "Lck498!!\r"}
      }
      expect "*password:" {send "Lck498!!\r"}
      expect eof
EOF
done



