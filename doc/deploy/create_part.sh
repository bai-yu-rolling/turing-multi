set -e;
dateYear=$(date -d '+1 years' +%Y-01-01);

for month_index in {1..12} ; do
    month_index_start=$(( $month_index - 1 ));
    dateStart=$(date -d "${dateYear} +${month_index_start} months" +%Y-%m-%d)
    dateUp1=$(date -d "${dateYear} +${month_index_start} months" +%Y_%m_%d)
    dateEnd=$(date -d "${dateYear} +${month_index} months" +%Y-%m-%d)
    echo "创建${dateStart}到${dateEnd}分区";
    PGPASSWORD='Univalsoft_2022' psql -U 'postgres' -c "create table qh_na_record_${dateUp1}  PARTITION of qh_na_record for values FROM ('${dateStart}') to ('${dateEnd}');" -p 5555 -d 'qh-detection-management';

done
