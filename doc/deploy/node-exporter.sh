#!/bin/bash


temp=`rpm -qa|grep -i golang|wc -l`
if [ ${temp} -eq '0' ]
then
  echo ">>>正在安装golang"
  yum -y install ./golang-1.15.14-1.el7.x86_64.rpm
fi
temp=`find /usr/local/bin/node_exporter|grep -i golang|wc -l`
if [ ${temp} -ne '0' ]
then
  echo ">>>已安装node_exporter"
  exit 0
fi
echo ">>>安装node_exporter"
tar xzvf ./node_exporter-1.0.1.linux-amd64.tar.gz -C /opt/
ln -s /opt/node_exporter-1.0.1.linux-amd64/node_exporter /usr/local/bin

echo ">>>安装服务"
cat > /usr/lib/systemd/system/node-exporter.service <<-'EOF'
[Unit]
Description=This is prometheus node exporter
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/node_exporter
ExecReload=/bin/kill -s -HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
echo ">>>启动服务"
systemctl daemon-reload && systemctl enable node-exporter.service && systemctl start node-exporter.service
echo ">>>开放端口服务"
# 端口 9100
firewall-cmd --add-port=9100/tcp --permanent
firewall-cmd --reload
echo ">>>安装结束"