#!/bin/bash
while getopts f: OPT; do
  case $OPT in
  f | +f)
    files="$OPTARG $files"
    ;;
  *)
    echo "usage: $(basename $0) [-f hostfile] <from> <to>"
    exit 2
    ;;
  esac
done
shift $(expr $OPTIND - 1)

if [ "" = "$files" ]; then
  echo "usage: $(basename $0) [-f hostfile] <from> <to>"
  exit
fi

for file in $files; do
  if [ ! -f "$file" ]; then
    echo "no hostlist file:$file"
    exit
  fi
  hosts="$hosts $(cat $file)"
done


command=$1
do_execute(){
  local host=$1
  local command=$2
  echo "do $host"
  ssh -p 9922 root@$host $command &
  wait
  echo "end $host"
}

for host in $hosts; do
  do_execute $host "$command" &
done

echo "异步执行中......"
wait
echo "$command 命令执行完毕"