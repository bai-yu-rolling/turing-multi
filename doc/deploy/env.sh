#!/bin/bash
while getopts e: OPT; do
  case $OPT in
  e | +e)
    env="$OPTARG"
    ;;
  ?)
    echo "usage: $(basename $0) <-e 环境>"
    exit 2
    ;;
  esac
done
shift $(expr $OPTIND - 1)

path=`${pwd}`
version=`cat /etc/redhat-release|sed -r 's/.* ([0-9]+)\..*/\1/'`
echo ">>>当前系统版本${version}"
if [ ${version} -ne '7' ]
then
  echo "只支持centos7"
  exit 0
fi

echo ">>>校验是否安装unzip"
temp=`rpm -qa|grep -i unzip|wc -l`
if [ ${temp} -eq '0' ]
then
  echo ">>>正在安装unzip"
  yum -y install ./unzip-6.0-22.el7_9.x86_64.rpm
fi

echo ">>>校验是否安装java"
temp=`java -version |& grep 1.8 | wc -l`
if [ ${temp} -eq '0' ]
then
  echo ">>>正在安装java到/usr/local/"
  tar -xzvf jdk-8u141-linux-x64.tar.gz -C /usr/local/
  echo ">>>软连接jar java 到 /usr/local/sbin"
  ln -s /usr/local/jdk1.8.0_141/bin/jar /usr/local/sbin/
  ln -s /usr/local/jdk1.8.0_141/bin/java /usr/local/sbin/
fi


echo ">>>防火墙放开端口：80、8080、443"
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --reload

echo ">>>配置应用程序"
echo ">>>准备存储路径，安装启动文件"
mkdir -p /mydata/jars
mkdir -p /mydata/sh
chmod +x ./systemctl-service-start.sh
cp ./systemctl-service-start.sh /mydata/sh
ln -s /mydata/sh/systemctl-service-start.sh /usr/local/sbin/

echo ">>>安装服务 *.service文件到 /usr/lib/systemd/system/"
#默认前端
cp ./monomer-web.service /usr/lib/systemd/system/
cp ./$env/*.service /usr/lib/systemd/system/
systemctl daemon-reload


echo ">>>安装完毕"
