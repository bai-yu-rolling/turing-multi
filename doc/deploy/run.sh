#第一步上传待部署文件到10.30.28.25服务器temp文件夹中

echo '第二步，将25jar包传递到各个服务器'

./doc/scp-transfer.sh -f hostsManage/hosts ./temp/  /mydata/

echo '第三步 使用执行脚本将各服务器jar包将temp中文件copy成四份'
 ./doc/ssh-execute-asyn.sh -f hostsManage/hosts  "cd /mydata/temp; ./copyService.sh"


echo '第四步 将各服务器jar包mv到指定文件夹'
 ./doc/ssh-execute-asyn.sh -f hostsManage/hosts "mv /mydata/temp/* /mydata/jars"
 
echo '第五步分步骤启动所有服务'

echo '启动所有前端开始'
./doc/ssh-execute-asyn.sh -f hostsManage/appletWebHosts "systemctl restart monomer-web.service"
echo '启动app前端结束'
./doc/ssh-execute-asyn.sh -f hostsManage/countyWebHosts "systemctl restart monomer-web.service"

echo '启动所有后端开始'
./doc/ssh-execute-asyn.sh -f hostsManage/appletCenter9002-9004Hosts "systemctl restart monomer-center900*.service"
echo '启动appletCenter3结束'
./doc/ssh-execute-asyn.sh -f hostsManage/appletCenter9002-9005Hosts "systemctl restart monomer-center900*.service"
echo '启动appletCenter4结束'
./doc/ssh-execute-asyn.sh -f hostsManage/countyCenter9002-9004Hosts "systemctl restart monomer-center900*.service"
echo '启动countyCenter3结束'
./doc/ssh-execute-asyn.sh -f hostsManage/countyCenter9002-9005Hosts "systemctl restart monomer-center900*.service"

echo '启动结束'